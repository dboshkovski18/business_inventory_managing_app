package com.inventmanagementapp.inventmanagementapp.domain.dto

data class ResellerDto(var reseller_id: Long?,
                       val reseller_name: String?,
                       val phone_number: String?,
                       val brand_name: String?)
