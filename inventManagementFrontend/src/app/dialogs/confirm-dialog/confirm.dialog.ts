import { Component, Inject, OnInit } from '@angular/core';
import { MatLegacyDialog as MatDialog, MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';

@Component({
  selector: 'ConfirmDialog',
  templateUrl: './confirm.dialog.html',
  styleUrls: ['./confirm.dialog.css']
})
export class ConfirmDialog implements OnInit {

  confirmDialogBody! : Confirmation

  constructor(@Inject(MAT_DIALOG_DATA) data : Confirmation,
              private _dialog : MatDialogRef<ConfirmDialog>) { 
                this.confirmDialogBody = data
              }

  ngOnInit(): void {
  }

  onConfirm() { 
    this._dialog.close(true)
  }

  onDecline() {
    this._dialog.close(false)
  }

  onClose() {
    this._dialog.close()
  }





}

export interface Confirmation{
  message: string,
  confirm_message: string,
  decline_message: string
}
