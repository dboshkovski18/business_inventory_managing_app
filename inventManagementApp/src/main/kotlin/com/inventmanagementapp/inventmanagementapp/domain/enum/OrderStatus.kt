package com.inventmanagementapp.inventmanagementapp.domain.enum

enum class OrderStatus {

    OPENED,
    NOT_OPENED

}