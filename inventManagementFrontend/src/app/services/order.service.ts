import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from '../interfaces/order';

@Injectable({
  providedIn: 'root'
})
export class OrderService {


  readonly path = '/api/orders'

  constructor(private http : HttpClient) { }


  processOrder(order : Order) : Observable<Order> {
    return this.http.post<Order>(`${this.path}`, order)
  }

  findAllOrdersByReseller(reseller_id : number): Observable<Order[]>{
    return this.http.get<Order[]>(`${this.path}/by-reseller/${reseller_id}`)
  }


  findAllOrdersByBusiness(business_id : number): Observable<Order[]>{
    return this.http.get<Order[]>(`${this.path}/by-business/${business_id}`)
  }

  filterOrdersByBusiness(business_id : number,date : string): Observable<Order[]>{
    return this.http.get<Order[]>(`${this.path}/filter-by-date?business_id=${business_id}&date=${date}`)
  }



  markOrderAsOpened(order_id : number): Observable<any>{
    return this.http.get<any>(`${this.path}/open/${order_id}`)
  }

  countResellersUnopenedOrders(reseller_id : number): Observable<number>{
    return this.http.get<number>(`${this.path}/not-opened/${reseller_id}`)
  }
}
