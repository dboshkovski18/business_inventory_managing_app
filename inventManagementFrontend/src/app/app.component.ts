import { Component } from '@angular/core';
import { TokenStorageService } from './services/token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'inventManagementFrontend';

  private role: string | undefined;

  isLoggedIn = false;
  username: string | undefined;

  constructor(private tokenStorageService: TokenStorageService) {
  }

  ngOnInit(): void {
    this.isLoggedIn = !!this.tokenStorageService.getToken()

    if(this.isLoggedIn){
      const user = this.tokenStorageService.getUser();
      this.role = user.role

      this.username = user.username
    }
}
}
