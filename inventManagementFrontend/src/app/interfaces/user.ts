export interface User{
    id?: number,
    username: string,
    password: string,
    role: string,
    business?: {
        id?: number,
        business_name: string
    },
    reseller?: {
        id?: number,
        reseller_name: string,
        phone_number: string,
        brand_name: string
    }
    enabled?: boolean,
    authorities?: string[] ,
    credentialsNonExpired?: boolean,
    accountNonExpired?: boolean,
    accountNonLocked?: boolean
}
