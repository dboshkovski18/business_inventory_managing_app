package com.inventmanagementapp.inventmanagementapp.service

import com.inventmanagementapp.inventmanagementapp.domain.Inventory
import com.inventmanagementapp.inventmanagementapp.domain.dto.*
import com.inventmanagementapp.inventmanagementapp.repo.BusinessRepo
import com.inventmanagementapp.inventmanagementapp.repo.InventoryRepo
import com.inventmanagementapp.inventmanagementapp.repo.ProductRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.persistence.EntityManager
import javax.persistence.Query

@Service
class InventoryService(
    private val inventoryRepository: InventoryRepo,
    private val businessRepo: BusinessRepo,
    private val productRepo: ProductRepo,
    private val entityManager: EntityManager
) {

    fun createInventory(inventoryDTO: InventoryDto): InventoryDto {
        val inventory = Inventory(
            quantity_limit = inventoryDTO.quantity_limit!!,
            business = this.businessRepo.findById(inventoryDTO.business?.business_id!!).get(),
            product = this.productRepo.findById(inventoryDTO.product?.id!!).get(),
            quantity_on_stock = null
        )
        val savedInventory = inventoryRepository.save(inventory)
        return InventoryDto(
            inventory_id = savedInventory.id,
            quantity_limit = savedInventory.quantity_limit,
            quantity_on_stock = savedInventory.quantity_on_stock,
            business = BusinessDto(savedInventory.business.id, savedInventory.business.business_name),
            product = ProductDto(
                savedInventory.product.id, savedInventory.product.product_name,
                ResellerDto(
                    savedInventory.product.reseller.id,
                    savedInventory.product.reseller.reseller_name,
                    savedInventory.product.reseller.phone_number,
                    savedInventory.product.reseller.brand_name
                )
            ),
            status = savedInventory.status.toString()
        )
    }

    fun getInventoryById(inventoryId: Long): InventoryDto? {
        val inventory = inventoryRepository.findById(inventoryId).orElse(null)
        return inventory?.let {
            InventoryDto(
                inventory_id = it.id,
                quantity_limit = it.quantity_limit,
                quantity_on_stock = it.quantity_on_stock,
                business = BusinessDto(it.business.id, it.business.business_name),
                product = ProductDto(
                    it.product.id, it.product.product_name,
                    ResellerDto(
                        it.product.reseller.id,
                        it.product.reseller.reseller_name,
                        it.product.reseller.phone_number,
                        it.product.reseller.brand_name
                    )
                ),
                status = it.status.toString()
            )
        }
    }

    fun getAllInventories(): List<InventoryDto> {
        val inventories = inventoryRepository.findAll()
        return inventories.map {
            InventoryDto(
                inventory_id = it.id,
                quantity_limit = it.quantity_limit,
                quantity_on_stock = it.quantity_on_stock,
                business = BusinessDto(it.business.id, it.business.business_name),
                product = ProductDto(
                    it.product.id, it.product.product_name,
                    ResellerDto(
                        it.product.reseller.id,
                        it.product.reseller.reseller_name,
                        it.product.reseller.phone_number,
                        it.product.reseller.brand_name
                    )
                ),
                status = it.status.toString()
            )
        }
    }

    fun updateInventory(inventoryDTO: InventoryDto): InventoryDto {
        val inventory = Inventory(
            id = inventoryDTO.inventory_id!!,
            quantity_limit = inventoryDTO.quantity_limit!!,
            business = this.businessRepo.findById(inventoryDTO.business?.business_id!!).get(),
            product = this.productRepo.findById(inventoryDTO.product?.id!!).get(),
            quantity_on_stock = null
        )
        val updatedInventory = inventoryRepository.save(inventory)
        return InventoryDto(
            inventory_id = updatedInventory.id,
            quantity_limit = updatedInventory.quantity_limit,
            quantity_on_stock = updatedInventory.quantity_on_stock,
            business = BusinessDto(updatedInventory.business.id, updatedInventory.business.business_name),
            product = ProductDto(
                updatedInventory.product.id, updatedInventory.product.product_name,
                ResellerDto(
                    updatedInventory.product.reseller.id,
                    updatedInventory.product.reseller.reseller_name,
                    updatedInventory.product.reseller.phone_number,
                    updatedInventory.product.reseller.brand_name
                )
            ),
            status = updatedInventory.status.toString()
        )
    }

    fun deleteInventory(inventoryId: Long) {
        inventoryRepository.deleteById(inventoryId)
    }

    fun getAllInventoriesByBusinessId(id : Long): List<InventoryDto> {
        val inventories = inventoryRepository.findAllByBusinessId(id)
        return inventories.map {
            InventoryDto(
                inventory_id = it.id,
                quantity_limit = it.quantity_limit,
                quantity_on_stock = it.quantity_on_stock,
                business = BusinessDto(it.business.id, it.business.business_name),
                product = ProductDto(
                    it.product.id, it.product.product_name,
                    ResellerDto(
                        it.product.reseller.id,
                        it.product.reseller.reseller_name,
                        it.product.reseller.phone_number,
                        it.product.reseller.brand_name
                    )
                ),
                status = it.status.toString()
            )
        }
    }


    fun checkIfBusinessHasProduct(business_id : Long, product_id : Long) : Boolean {
        return this.inventoryRepository.checkCombinationExists(business_id,product_id)
    }

    fun deleteProductByBusiness(business_id: Long, product_id: Long) {
        return this.inventoryRepository.deleteByBusinessIdAndProductId(business_id,product_id)
    }

    fun updateQuantityOnStock(inventory_id: Long,quantity : Int): Inventory {
        val inventory = this.inventoryRepository.findById(inventory_id).get()

        inventory.quantity_on_stock = quantity

        return this.inventoryRepository.save(inventory)
    }


}
