package com.inventmanagementapp.inventmanagementapp.repo

import com.inventmanagementapp.inventmanagementapp.domain.Inventory
import com.inventmanagementapp.inventmanagementapp.domain.dto.ProductDto
import javax.transaction.Transactional
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface InventoryRepo : JpaRepository<Inventory, Long> {

    fun findAllByBusinessId(business_id: Long): List<Inventory>

    @Query(
        value = "SELECT EXISTS (SELECT 1 FROM inventories WHERE business_id = :businessId AND product_id = :productId)",
        nativeQuery = true
    )
    fun checkCombinationExists(businessId: Long, productId: Long): Boolean

    @Transactional
    @Modifying
    @Query(
        value = "DELETE FROM inventories WHERE business_id = :businessId AND product_id = :productId",
        nativeQuery = true
    )
    fun deleteByBusinessIdAndProductId(businessId: Long, productId: Long)

    fun findByBusinessIdAndProductId(business_id: Long, product_id : Long) : Inventory


}