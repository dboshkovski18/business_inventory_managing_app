import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../interfaces/user";
import {LogInResponse} from "../interfaces/login-response";
import {FormControl, FormGroup} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private http: HttpClient) {
  }

  readonly path = '/api/register'

  logIn(username: string, password: string) : Observable<LogInResponse> {
    return this.http.post<LogInResponse>("/login", {
      "username" : username,
      "password" : password
    })
  }

  registerBusiness(form : any): any{
    console.log(form)

    var user : User = {
      username : form.username,
      password  : form.password,
      role : 'ROLE_BUSINESS',
        business : {
          business_name : form.business_name
        }
    }

    return this.http.post<any>(this.path,user)
  }


  registerReseller(form : any): any {

    var user : User = {
      username : form.username,
      password  : form.password,
      role : 'ROLE_RESELLER',
        reseller : {
          reseller_name : form.reseller_name,
          phone_number: form.phone_number,
          brand_name: form.brand_name
        }
    }

    return this.http.post<any>(this.path,user)
  }

  

}
