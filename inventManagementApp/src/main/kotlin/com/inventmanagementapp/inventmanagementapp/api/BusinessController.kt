package com.inventmanagementapp.inventmanagementapp.api


import com.inventmanagementapp.inventmanagementapp.service.BusinessService
import com.inventmanagementapp.inventmanagementapp.domain.dto.BusinessDto
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/businesses")
class BusinessController (
    private val businessService: BusinessService
) {

    @GetMapping("/all")
    fun getAllBusinesses(): List<BusinessDto> {
        return businessService.getAllBusinesses()
    }

    @GetMapping("/{businessId}")
    fun getBusinessById(@PathVariable businessId: Long): BusinessDto? {
        return businessService.getBusinessById(businessId)
    }

    @PostMapping("/create")
    fun createBusiness(@RequestBody businessDTO: BusinessDto): BusinessDto {
        return businessService.createBusiness(businessDTO)
    }

    @PutMapping("/edit/{businessId}")
    fun updateBusiness(
        @PathVariable businessId: Long,
        @RequestBody businessDTO: BusinessDto
    ): BusinessDto {
        businessDTO.business_id = businessId
        return businessService.updateBusiness(businessDTO)
    }

    @DeleteMapping("/delete/{businessId}")
    fun deleteBusiness(@PathVariable businessId: Long) {
        businessService.deleteBusiness(businessId)
    }
}
