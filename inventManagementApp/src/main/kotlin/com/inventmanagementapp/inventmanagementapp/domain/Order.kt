package com.inventmanagementapp.inventmanagementapp.domain

import com.inventmanagementapp.inventmanagementapp.domain.enum.OrderStatus
import com.inventmanagementapp.inventmanagementapp.domain.enum.OrderUrgency
import java.time.LocalDate
import javax.persistence.*


@Entity
@Table(name = "orders")
data class Order(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) val id: Long? = 0,

    @ManyToOne
    @JoinColumn(name = "product_id") val product: Product? = null,

    val quantityOrdered : Int = 0,


    val orderDate: LocalDate? = LocalDate.now(),

    @Enumerated(value = EnumType.STRING) val urgency: OrderUrgency?,

    @Enumerated(value = EnumType.STRING) var status: OrderStatus? = OrderStatus.NOT_OPENED,

    @ManyToOne
    @JoinColumn(name = "reseller_id") val reseller: Reseller? = null,

    @ManyToOne
    @JoinColumn(name = "business_id") val business: Business? = null
)




