import { Reseller } from "./reseller";

export interface Product {
    id: number,
    product_name: string,
    reseller : Reseller
}
