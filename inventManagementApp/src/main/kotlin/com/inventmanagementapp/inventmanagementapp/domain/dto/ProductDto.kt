package com.inventmanagementapp.inventmanagementapp.domain.dto

import com.inventmanagementapp.inventmanagementapp.domain.Reseller

data class ProductDto(
    var id: Long?,
    val product_name: String?,
    val reseller: ResellerDto?
)
