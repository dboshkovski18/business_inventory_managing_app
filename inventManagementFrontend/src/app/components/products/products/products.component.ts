import { Component, OnInit } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { debounceTime, distinctUntilChanged, Observable, Subject, switchMap } from 'rxjs';
import { ConfirmDialog } from 'src/app/dialogs/confirm-dialog/confirm.dialog';
import { CreateProductDialog } from 'src/app/dialogs/create-product.dialog /create-product.dialog';
import { EditProductDialog } from 'src/app/dialogs/edit-product.dialog/edit-product.dialog';
import { Product } from 'src/app/interfaces/product';
import { InventoryService } from 'src/app/services/inventory.service';
import { ProductService } from 'src/app/services/product.service';
import { Inventory } from 'src/app/interfaces/inventory';
import { BusinessService } from 'src/app/services/business.service';
import { CreateInventoryDialog } from 'src/app/dialogs/create-inventory.dialog /create-inventory.dialog';
import { User } from 'src/app/interfaces/user';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { ProductResponse } from 'src/app/interfaces/product-response';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  user! : User
  loader! : boolean

  products! : ProductResponse[]


  pageSize = 5; // Number of items per page
  currentPage = 1;

  private searchTerms = new Subject<string>()
  searchForm! : FormGroup


  errorMessage : boolean = false


  constructor(private productService : ProductService
    ,private dialog : MatDialog,
    private inventoryService: InventoryService,
    private businessService: BusinessService,
    private tokenService: TokenStorageService) {     

      this.searchForm = new FormGroup({
        searchTerm : new FormControl()

      })

      this.searchForm.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(term => {
          this.loader = true 
          return this.productService.searchProductsByName(this.searchForm.value.searchTerm, this.user.business?.id!!)
          
        })
      )
      .subscribe(data => {
        console.log(data)
        this.products = data
        if(data.length == 0){
          this.errorMessage = true
        }else{
          this.errorMessage = false
        }

        this.loader = false
      },()=>{})
    }

     

  ngOnInit(): void {
    this.user = this.tokenService.getUser()
    this.loader = true
    this.loadAllProducts()
  }

  searchByTerm(term : string) {
    this.productService.searchProductsByName(term, this.user.business?.id!!).subscribe(data => {
      this.products = data
    })
  }

  get paginatedItems() {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    return this.products.slice(startIndex, endIndex);
  }

  changePage(pageNumber: number) {
    this.currentPage = pageNumber;
  }

  getPageNumbers() {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(this.products.length / this.pageSize); i++) {
      pageNumbers.push(i);
    }
    return pageNumbers;
  }


  loadAllProducts() {
    this.loader = false
    this.productService.findAllByBusiness(this.user.business!!.id!!).subscribe(data => {
      this.products = data
    })
  }

  openAddProductDialog() {
    this.dialog
    .open(CreateProductDialog)
    .afterClosed().subscribe(() => {
      this.loader = true
      this.loadAllProducts()
    })
  } 

  askToDelete(id : number) {
    this.dialog
    .open(ConfirmDialog, {
      data : { 
        message: "Are you sure you want to delete this reseller?",
        confirm_message: "Yes",
        decline_message: "No"
      }
      })
    .afterClosed()
    .subscribe( data => {
      if(data){
        this.productService.deleteById(id).subscribe(() => {
          this.loader = true
          this.loadAllProducts()
        })
      }
    })
  }


  editProduct(product : Product) {
    this.dialog.open(EditProductDialog, {
      data : {
        product: product
      }
    })
    .afterClosed()
    .subscribe(() => {
      this.loader = true
      this.loadAllProducts()
    })
  } 

  addToBusiness(product : Product){
    
    this.dialog.open(CreateInventoryDialog, { data : { product : product }})
    .afterClosed()
    .subscribe(complete => {
      this.loader = true
      this.loadAllProducts()
    })
  }

  removeFromBusiness(product : Product) {


    console.log(this.inventoryService.checkIfBusinessHasProduct(this.user.business!!.id!!,product.id))



    this.dialog.open(ConfirmDialog,{
      data : { 
        message: "Are you sure you want to delete this reseller?",
        confirm_message: "Yes",
        decline_message: "No"
      }
      })
    .afterClosed()
    .subscribe( data => {
      if(data){
        this.inventoryService.removeProductFromBusiness(this.user.business!!.id!!,product.id).subscribe(complete => {
          this.loader = true
          this.loadAllProducts()
        })
      }
      
        })
  }


}
