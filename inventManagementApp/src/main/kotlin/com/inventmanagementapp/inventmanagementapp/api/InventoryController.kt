package com.inventmanagementapp.inventmanagementapp.api


import com.inventmanagementapp.inventmanagementapp.domain.Inventory
import com.inventmanagementapp.inventmanagementapp.domain.dto.InventoryDto
import com.inventmanagementapp.inventmanagementapp.service.InventoryService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/inventories")
class InventoryController(
    private val inventoryService: InventoryService
) {

    @GetMapping("/all")
    fun getAllInventories(): List<InventoryDto> {
        return inventoryService.getAllInventories()
    }

    @GetMapping("/{inventoryId}")
    fun getInventoryById(@PathVariable inventoryId: Long): InventoryDto? {
        return inventoryService.getInventoryById(inventoryId)
    }

    @PostMapping("/create")
    fun createInventory(@RequestBody inventoryDTO: InventoryDto): InventoryDto {
        return inventoryService.createInventory(inventoryDTO)
    }

    @PutMapping("/edit/{inventoryId}")
    fun updateInventory(
        @PathVariable inventoryId: Long,
        @RequestBody inventoryDTO: InventoryDto
    ): InventoryDto {
        inventoryDTO.inventory_id = inventoryId
        return inventoryService.updateInventory(inventoryDTO)
    }

    @DeleteMapping("/delete/{inventoryId}")
    fun deleteInventory(@PathVariable inventoryId: Long) {
        inventoryService.deleteInventory(inventoryId)
    }

    @GetMapping("/all-by-business/{id}")
    fun getAllInventoriesByBusiness(@PathVariable id: Long): List<InventoryDto> {
        return inventoryService.getAllInventoriesByBusinessId(id)
    }

    @GetMapping("/checkIfBusinessHasProduct")
    fun checkIfBusinessHasProduct(@RequestParam business_id: Long, @RequestParam product_id: Long): Boolean =
        this.inventoryService.checkIfBusinessHasProduct(business_id, product_id)


    @DeleteMapping("/deleteProductFromBusiness")
    fun deleteProductFromBusiness(@RequestParam business_id: Long, @RequestParam product_id: Long) =
        this.inventoryService.deleteProductByBusiness(business_id, product_id)


    @GetMapping("/updateQuantityOnStock")
    fun updateQuantityOnStock(@RequestParam inventory_id: Long, @RequestParam quantity: Int): Any =
        this.inventoryService.updateQuantityOnStock(inventory_id, quantity)

}
