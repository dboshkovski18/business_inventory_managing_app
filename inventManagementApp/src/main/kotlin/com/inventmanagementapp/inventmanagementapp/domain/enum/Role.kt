package com.inventmanagementapp.inventmanagementapp.domain.enum

import org.springframework.security.core.GrantedAuthority

enum class Role : GrantedAuthority{
    ROLE_ADMIN,
    ROLE_BUSINESS,
    ROLE_RESELLER;

    override fun getAuthority(): String {
        return name
    }
}