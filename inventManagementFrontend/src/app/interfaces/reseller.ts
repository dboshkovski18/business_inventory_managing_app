export interface Reseller {
    reseller_id: number,
    reseller_name: string,
    phone_number: string,
    brand_name: string
}
