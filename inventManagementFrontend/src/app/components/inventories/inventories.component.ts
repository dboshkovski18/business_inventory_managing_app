import { Component, OnInit } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { Observable } from 'rxjs';
import { UpdateStockInventoryDialog } from 'src/app/dialogs/edit-quantity.dialog /edit-quantity.dialog';
import { OrderDialog } from 'src/app/dialogs/order.dialog/order.dialog';
import { Inventory } from 'src/app/interfaces/inventory';
import { Order } from 'src/app/interfaces/order';
import { User } from 'src/app/interfaces/user';
import { BusinessService } from 'src/app/services/business.service';
import { InventoryService } from 'src/app/services/inventory.service';
import { OrderService } from 'src/app/services/order.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-inventories',
  templateUrl: './inventories.component.html',
  styleUrls: ['./inventories.component.css']
})
export class InventoriesComponent implements OnInit {

  user! : User

  loader! : boolean
  
  ordersByBusiness! : Observable<Order[]>

  inventories! : Inventory[]

  pageSize = 3; // Number of items per page
  currentPage = 1;

  selectedDate!: string;
  pastDays: string[] = [];


 
  constructor(private inventoryService: InventoryService,
    private dialog : MatDialog,
    private tokenStorageService: TokenStorageService,
    private orderService : OrderService,
    private businessService: BusinessService
    ) {
      this.populatePastDays()
     }

  ngOnInit(): void {
    this.user = this.tokenStorageService.getUser()
    this.loadAllInventories()
    this.loadAllOrders()
  }

  populatePastDays() {
    const today = new Date();
    for (let i = 0; i < 5; i++) {
      const pastDate = new Date(today);
      pastDate.setDate(today.getDate() - i);
      const formattedDate = this.formatDate(pastDate);
      this.pastDays.push(formattedDate);
    }
  }

  formatDate(date: Date): string {
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
  }

  get paginatedItems() {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    return this.inventories.slice(startIndex, endIndex);
  }

  changePage(pageNumber: number) {
    this.currentPage = pageNumber;
  }

  getPageNumbers() {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(this.inventories.length / this.pageSize); i++) {
      pageNumbers.push(i);
    }
    return pageNumbers;
  }


  loadAllOrders(){
  
      this.ordersByBusiness =this.orderService.filterOrdersByBusiness(this.user.business!!.id!!,this.formatDate(new Date))
  }

  loadAllInventories() {
    this.loader = true

    this.inventoryService.getAllInventoriesByBusiness(this.user.business!!.id!!).subscribe(data => {
      this.inventories = data
    },(error) => {}, () => {
      this.loader = false
    })
  } 

  openQuantityStockDialog(inventory: Inventory) {
    this.dialog.open(UpdateStockInventoryDialog, {data : {inventory : inventory}})
    .afterClosed().subscribe(()=> {
      this.loadAllInventories()
    })
  }

  openOrderDialog(inventory: Inventory){
      this.dialog.open(OrderDialog, {data : { inventory : inventory}}).afterClosed()
      .subscribe(() => {
        this.loadAllOrders()
        this.loadAllInventories()
      })
  
  }

  onDateChange() {
    this.ordersByBusiness = this.orderService.filterOrdersByBusiness(this.user.business?.id!!,this.selectedDate)
  }

}
