import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class ProductResellerService {

  readonly path = '/api/product-resellers'


  constructor(private _http: HttpClient) { }

  findAllProductsByResellerName(name: string) : Observable<Product[]>{
    return this._http.get<Product[]>(`${this.path}/by-reseller/${name}`)
  }

  searchProductsByReseller(term : string, reseller_id: number) : Observable<Product[]> {
    return this._http.get<Product[]>(`${this.path}/search?term=${term}&reseller_id=${reseller_id}`)
  }

}
