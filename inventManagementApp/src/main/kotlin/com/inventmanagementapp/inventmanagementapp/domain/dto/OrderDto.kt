package com.inventmanagementapp.inventmanagementapp.domain.dto

import com.inventmanagementapp.inventmanagementapp.domain.Business
import com.inventmanagementapp.inventmanagementapp.domain.Product
import com.inventmanagementapp.inventmanagementapp.domain.Reseller
import com.inventmanagementapp.inventmanagementapp.domain.enum.OrderUrgency
import java.time.LocalDate
import javax.persistence.*

data class OrderDto(
    val id: Long?,
    val product: Product ,
    val quantityOrdered  : Int,
    val orderDate: String?,
    val urgency: String,
    val status : String?,
    val business: Business
)
