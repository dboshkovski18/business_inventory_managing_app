package com.inventmanagementapp.inventmanagementapp.domain.dto

import com.inventmanagementapp.inventmanagementapp.domain.enum.Role

class LoginRequest(val username:String? = null , val password:String? = null , val role: Role?=null){}