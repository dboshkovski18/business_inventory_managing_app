package com.inventmanagementapp.inventmanagementapp.service

import com.inventmanagementapp.inventmanagementapp.domain.Reseller
import com.inventmanagementapp.inventmanagementapp.domain.dto.ResellerDto
import com.inventmanagementapp.inventmanagementapp.repo.ResellerRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ResellerService (private val resellerRepository: ResellerRepo) {

    fun createReseller(resellerDTO: ResellerDto): ResellerDto{
        val reseller = Reseller(
            reseller_name = resellerDTO.reseller_name!!,
            phone_number = resellerDTO.phone_number!!,
            brand_name = resellerDTO.brand_name!!
        )
        val savedReseller = resellerRepository.save(reseller)
        return ResellerDto(
            reseller_id = savedReseller.id,
            reseller_name = savedReseller.reseller_name,
            phone_number = savedReseller.phone_number,
            brand_name = savedReseller.brand_name
        )
    }

    fun getResellerById(resellerId: Long): ResellerDto? {
        val reseller = resellerRepository.findById(resellerId).orElse(null)
        return reseller?.let {
            ResellerDto(
                reseller_id = it.id,
                reseller_name = it.reseller_name,
                phone_number = it.phone_number,
                brand_name = it.brand_name
            )
        }
    }

    fun getAllResellers(): List<ResellerDto> {
        val resellers = resellerRepository.findAll()
        return resellers.map {
            ResellerDto(
                reseller_id = it.id,
                reseller_name = it.reseller_name,
                phone_number = it.phone_number,
                brand_name = it.brand_name
            )
        }
    }

    fun updateReseller(resellerDTO: ResellerDto): ResellerDto {
        val reseller = Reseller(
            id = resellerDTO.reseller_id!!,
            reseller_name = resellerDTO.reseller_name!!,
            phone_number = resellerDTO.phone_number!!,
            brand_name = resellerDTO.brand_name!!
        )
        val updatedReseller = resellerRepository.save(reseller)
        return ResellerDto(
            reseller_id = updatedReseller.id,
            reseller_name = updatedReseller.reseller_name,
            phone_number = updatedReseller.phone_number,
            brand_name = updatedReseller.brand_name
        )
    }

    fun deleteReseller(resellerId: Long) {
        resellerRepository.deleteById(resellerId)
    }

    fun getResellersByIds(resellerIDs: List<Long>): List<Reseller> {
        return resellerRepository.findAllById(resellerIDs)
    }

    fun searchResellersByNameOrBrand(term : String) : List<ResellerDto?>{
        val listOfResellers : List<Reseller?> =  this.resellerRepository.searchByResellerNameOrBrand(term.toLowerCase())

        return listOfResellers.map{ it -> ResellerDto(
            reseller_id = it!!.id,
            reseller_name = it.reseller_name,
            phone_number = it.phone_number,
            brand_name = it.brand_name
        )}

    }
}
