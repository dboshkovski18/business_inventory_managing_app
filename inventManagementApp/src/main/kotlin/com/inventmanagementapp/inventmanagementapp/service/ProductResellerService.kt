package com.inventmanagementapp.inventmanagementapp.service

import ResellerProduct
import com.inventmanagementapp.inventmanagementapp.domain.Product
import com.inventmanagementapp.inventmanagementapp.domain.ProductReseller
import com.inventmanagementapp.inventmanagementapp.domain.Reseller
import com.inventmanagementapp.inventmanagementapp.domain.dto.ProductResellerDto
import com.inventmanagementapp.inventmanagementapp.repo.ProductRepo
import com.inventmanagementapp.inventmanagementapp.repo.ProductResellerRepo
import com.inventmanagementapp.inventmanagementapp.repo.ResellerRepo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ProductResellerService (
    private val productResellerRepo: ProductResellerRepo,
    private val productRepo: ProductRepo,
    private val resellerRepo: ResellerRepo,
) {

    fun getAllProductResellers(): List<ProductReseller> {
        return productResellerRepo.findAll()
    }

    fun findProductsByReseller(resellerName: String): List<Product> {
        val productResellers = productResellerRepo.findAllByResellerName(resellerName)
        val productIDs = productResellers.map { it.id?.product_id }
        return productRepo.findAllById(productIDs)
    }

    fun findResellersByProduct(productName: String): List<Reseller> {
        val productResellers = productResellerRepo.findAllByProductName(productName)
        val resellerNames = productResellers.map { it.id?.reseller_id }
        return resellerRepo.findAllById(resellerNames)
    }

    fun searchProductsByReseller(term: String, resellerId : Long) : List<Product> {
        val productResellers =  this.productResellerRepo.searchProductsByReseller(term.toLowerCase(), resellerId)
        val productIds = productResellers.map { it.id?.product_id }
        return productRepo.findAllById(productIds)

    }
}