package com.inventmanagementapp.inventmanagementapp.api

import com.inventmanagementapp.inventmanagementapp.domain.Product
import com.inventmanagementapp.inventmanagementapp.domain.ProductReseller
import com.inventmanagementapp.inventmanagementapp.domain.Reseller
import com.inventmanagementapp.inventmanagementapp.domain.dto.ProductResellerDto
import com.inventmanagementapp.inventmanagementapp.service.ProductResellerService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/product-resellers")
class ProductResellerController @Autowired constructor(
    private val productResellerService: ProductResellerService // Inject your service
) {

    // Endpoint to retrieve all ProductReseller entities
    @GetMapping("/all")
    fun getAllProductResellers(): List<ProductReseller> {
        return productResellerService.getAllProductResellers()
    }

    // Endpoint to retrieve ProductReseller entities by reseller ID
    @GetMapping("/by-reseller/{resellerName}")
    fun getProductResellersByReseller(@PathVariable resellerName: String): List<Product> {
        return productResellerService.findProductsByReseller(resellerName)
    }

    // Endpoint to retrieve ProductReseller entities by product ID
    @GetMapping("/by-product/{productName}")
    fun getProductResellersByProduct(@PathVariable productName: String): List<Reseller> {
        return productResellerService.findResellersByProduct(productName)
    }

    @GetMapping("/search")
    fun searchProductsByReseller(@RequestParam term: String, @RequestParam reseller_id: Long) : List<Product> =
        this.productResellerService.searchProductsByReseller(term, reseller_id)

}
