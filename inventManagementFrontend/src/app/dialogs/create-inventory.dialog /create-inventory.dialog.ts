import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog'
import { CreateResellerComponent } from 'src/app/components/create-reseller/create-reseller.component';
import { Product } from 'src/app/interfaces/product';

@Component({
  selector: 'CreateInventoryDialog',
  templateUrl: './create-inventory.dialog.html',
  styleUrls: ['./create-inventory.dialog.css']
})
export class CreateInventoryDialog implements OnInit{
  
  
  product!: Product;

 

  constructor(public __dialogRef: MatDialogRef<CreateInventoryDialog>,
    @Inject (MAT_DIALOG_DATA) data : any) {
      this.product = data.product
     }

  ngOnInit(): void {

    
  }

  closeDialog() {
   this.__dialogRef.close()
  }

}
