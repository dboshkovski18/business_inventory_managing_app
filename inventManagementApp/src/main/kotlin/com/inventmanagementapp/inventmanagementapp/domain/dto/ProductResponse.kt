package com.inventmanagementapp.inventmanagementapp.domain.dto

import com.inventmanagementapp.inventmanagementapp.domain.Product
import com.inventmanagementapp.inventmanagementapp.domain.Reseller

class ProductResponse(var product: Product?,
                      val reseller: Reseller?,
                      val inStock: Boolean?)
