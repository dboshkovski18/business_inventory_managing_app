package com.inventmanagementapp.inventmanagementapp.service

import com.inventmanagementapp.inventmanagementapp.domain.Inventory
import com.inventmanagementapp.inventmanagementapp.domain.Order
import com.inventmanagementapp.inventmanagementapp.domain.Product
import com.inventmanagementapp.inventmanagementapp.domain.Reseller
import com.inventmanagementapp.inventmanagementapp.domain.dto.*
import com.inventmanagementapp.inventmanagementapp.domain.enum.InventoryStatus
import com.inventmanagementapp.inventmanagementapp.domain.enum.OrderStatus
import com.inventmanagementapp.inventmanagementapp.domain.enum.OrderUrgency
import com.inventmanagementapp.inventmanagementapp.repo.InventoryRepo
import com.inventmanagementapp.inventmanagementapp.repo.OrderRepo
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service
class OrderService(
    private val orderRepo: OrderRepo,
    private val inventoryRepo: InventoryRepo
) {


    fun findAllOrdersByReseller(reseller_id: Long): List<Order> {
        return this.orderRepo.findAllByResellerId(reseller_id)
    }

    fun findAllByBusiness(business_id: Long): List<Order> {
        return this.orderRepo.findAllByBusinessId(business_id)
    }

    fun findAllByBusinessIdAndOrderDate(business_id: Long, order_date: LocalDate): List<Order> {
        return this.orderRepo.findAllByBusinessIdAndOrderDate(business_id, order_date)
    }


    fun markOrderAsOpened(order_id: Long) {
        var order: Order = this.orderRepo.findById(order_id).get()
        var inventory: Inventory =
            this.inventoryRepo.findByBusinessIdAndProductId(order.business!!.id, order.product!!.id)
        order.status = OrderStatus.OPENED
        inventory.quantity_on_stock = inventory.quantity_limit
        inventory.status = InventoryStatus.NOT_ORDERED
        this.inventoryRepo.save(inventory)
        this.orderRepo.save(order)
    }

    fun countNotOpenedByReseller(reseller_id: Long): Int {
        return this.orderRepo.countNotOpenedByReseller(reseller_id)
    }

    fun processOrder(orderDto: OrderDto): OrderDto {
        val order = Order(
            product = orderDto.product,
            quantityOrdered = orderDto.quantityOrdered,
            reseller = orderDto.product.reseller,
            urgency = OrderUrgency.valueOf(orderDto.urgency),
            business = orderDto.business

        )
        val processedOrder = orderRepo.save(order)


        var inventory: Inventory =
            this.inventoryRepo.findByBusinessIdAndProductId(order.business!!.id, order.product!!.id)
        inventory.status = InventoryStatus.ORDERED
        this.inventoryRepo.save(inventory)

        return OrderDto(
            id = processedOrder.id,
            product = Product(
                id = processedOrder.product!!.id,
                product_name = processedOrder.product.product_name,
                reseller = processedOrder.reseller!!
            ),
            quantityOrdered = processedOrder.quantityOrdered,
            urgency = processedOrder.urgency.toString(),
            orderDate = processedOrder.orderDate.toString(),
            business = processedOrder.business!!,
            status = processedOrder.status.toString()
        )

    }
}