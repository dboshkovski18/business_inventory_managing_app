import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { TokenStorageService } from '../services/token-storage.service';

@Injectable({
  providedIn: 'root'
})
export class BusinessGuard  {


  constructor(private tokenStorageService: TokenStorageService, 
    private router : Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {


      if (!this.tokenStorageService.isBusiness()) {

        this.router.navigate(['/dashboard']) // go to login if not authenticated

        return false;

      }

    return true;
  }
  
}
