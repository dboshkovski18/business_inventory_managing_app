import { Component, Inject, OnInit } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog'
import { ResellerService } from 'src/app/services/reseller.service';
@Component({
  selector: 'DeleteResellerDialog',
  templateUrl: './delete-reseller.dialog.html',
  styleUrls: ['./delete-reseller.dialog.css']
})
export class DeleteResellerDialog implements OnInit {

  reseller_id! : number

  constructor(private __dialogRef : MatDialogRef<DeleteResellerDialog>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private resellerService: ResellerService) {
      this.reseller_id = data.id
     }

  ngOnInit(): void {
  }

  closeDialog() {
    this.__dialogRef.close()
  }

  confirmDelete() {
    this.resellerService.deleteById(this.reseller_id).subscribe()
    this.closeDialog()
  }

  
}
