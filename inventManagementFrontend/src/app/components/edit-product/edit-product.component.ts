import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ResellerService } from 'src/app/services/reseller.service';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog'
import { EditResellerDialog } from 'src/app/dialogs/edit-reseller.dialog/edit-reseller.dialog';
import { Reseller } from 'src/app/interfaces/reseller';
import { Product } from 'src/app/interfaces/product';
import { ProductService } from 'src/app/services/product.service';
import { EditProductDialog } from 'src/app/dialogs/edit-product.dialog/edit-product.dialog';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  editProductForm! : FormGroup


  loader: boolean = false
  errorMessage: boolean = false
  succesMessage: boolean = false

  
  productId! : number
  resellers! : Reseller[]

  @Input() product! : Product

  @Input() __dialogRef! : MatDialogRef<EditProductDialog>



  constructor(private resellerService: ResellerService, 
    private productService : ProductService,
    private tokenService: TokenStorageService ){ }

  ngOnInit(): void {
    
    this.resellerService.getAllResellers().subscribe(data => {
      this.resellers = data
    })

    this.editProductForm = new FormGroup({
      product_name : new FormControl(this.product.product_name)
    })

    this.productId = this.product.id
  }

  editProduct() {
    console.log(this.tokenService.getUser()) 



   this.resellerService.findById(this.tokenService.getUser()!!.reseller!!.id!!).subscribe(data => {

    var product : Product = {
      id: 0,
      product_name: this.editProductForm.value.product_name,
      reseller : data
    }

    this.productService.editProduct(this.productId, product).subscribe(()=> {},() => {
      this.errorMessage = true
    },() => {

      this.loader = false
      this.succesMessage = true
      
    })
   })

    
    

      
    

    
   

    this.__dialogRef.close()
  }

  
}
