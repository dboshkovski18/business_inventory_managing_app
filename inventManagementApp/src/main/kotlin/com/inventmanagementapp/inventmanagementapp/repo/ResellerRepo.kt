package com.inventmanagementapp.inventmanagementapp.repo

import com.inventmanagementapp.inventmanagementapp.domain.Reseller
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository


@Repository
interface ResellerRepo : JpaRepository<Reseller, Long> {


    @Query("SELECT r FROM Reseller r WHERE LOWER(r.reseller_name) LIKE %:searchValue% OR LOWER(r.brand_name) LIKE %:searchValue%")
    fun searchByResellerNameOrBrand(searchValue: String?): List<Reseller?>


}