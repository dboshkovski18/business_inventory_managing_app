package com.inventmanagementapp.inventmanagementapp.repo

import com.inventmanagementapp.inventmanagementapp.domain.ProductReseller
import com.inventmanagementapp.inventmanagementapp.domain.composite_key.ProductResellerID
import com.inventmanagementapp.inventmanagementapp.domain.dto.ProductResellerDto
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface ProductResellerRepo : JpaRepository<ProductReseller, ProductResellerID> {


    @Query("SELECT pr FROM ProductReseller pr WHERE pr.product_name = :productName")
    fun findAllByProductName(productName: String): List<ProductReseller>


    @Query("SELECT pr FROM ProductReseller pr WHERE pr.reseller_name = :resellerName")
    fun findAllByResellerName(resellerName: String): List<ProductReseller>


    @Query("SELECT pr FROM ProductReseller pr WHERE LOWER(pr.product_name) LIKE %:searchValue% AND pr.id.reseller_id = :resellerId")
    fun searchProductsByReseller(searchValue: String, resellerId :Long) : List<ProductReseller>


}