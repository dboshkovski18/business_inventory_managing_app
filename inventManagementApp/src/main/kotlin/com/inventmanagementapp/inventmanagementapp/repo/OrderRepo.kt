package com.inventmanagementapp.inventmanagementapp.repo

import com.inventmanagementapp.inventmanagementapp.domain.Business
import com.inventmanagementapp.inventmanagementapp.domain.Order
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
interface OrderRepo : JpaRepository<Order, Long> {

    fun findAllByResellerId(reseller_id: Long): List<Order>

    fun findAllByBusinessId(business_id: Long): List<Order>

    @Query(
        "SELECT COUNT(o) FROM orders o WHERE o.reseller_id = :resellerId AND o.status = 'NOT_OPENED'",
        nativeQuery = true
    )
    fun countNotOpenedByReseller(@Param("resellerId") resellerId: Long?): Int

    fun findAllByBusinessIdAndOrderDate(business: Long, order_date: LocalDate): List<Order>


}