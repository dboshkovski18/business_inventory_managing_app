import { Product } from "./product";
import { Reseller } from "./reseller";
import {User} from "./user";

export interface ProductResponse{
    product : Product,
    reseller : Reseller,
    inStock: boolean
}
