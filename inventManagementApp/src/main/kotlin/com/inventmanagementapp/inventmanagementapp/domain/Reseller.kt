package com.inventmanagementapp.inventmanagementapp.domain

import javax.persistence.*

@Entity
@Table (name= "resellers")
data class Reseller(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id : Long = 0,

    val reseller_name: String,
    val phone_number: String,
    val brand_name: String

)
