-- Create the Business table
CREATE TABLE businesses (
                          id bigserial PRIMARY KEY,
                          business_name VARCHAR(100)
);

-- Create the Reseller table
CREATE TABLE resellers (
                           id bigserial PRIMARY KEY,
                           reseller_name VARCHAR(100),
                           phone_number VARCHAR(20),
                           brand_name VARCHAR(100)
);


-- Create the Product table
CREATE TABLE products (
                         id bigserial PRIMARY KEY,
                         product_name VARCHAR(100),
                         reseller_id bigint,
                         FOREIGN KEY (reseller_id) REFERENCES resellers(id) on delete cascade
);


-- Create the Inventory table
CREATE TABLE inventories (
                           id bigserial PRIMARY KEY,
                           business_id bigint,
                           product_id bigint,
                           quantity_limit INT not null,
                           quantity_on_stock INT default null,
                           FOREIGN KEY (business_id) REFERENCES businesses(id) on delete cascade,
                           FOREIGN KEY (product_id) REFERENCES products(id) on delete cascade
);



CREATE OR REPLACE FUNCTION set_default_quantity_on_stock()
    RETURNS TRIGGER AS $$
BEGIN
    IF NEW.quantity_on_stock IS NULL THEN
        NEW.quantity_on_stock := NEW.quantity_limit;
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER set_default_quantity_on_stock_trigger
    BEFORE INSERT ON inventories
    FOR EACH ROW
EXECUTE FUNCTION set_default_quantity_on_stock();