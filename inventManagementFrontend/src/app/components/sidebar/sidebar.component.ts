import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { OrderService } from 'src/app/services/order.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  user! : User
  notificationsNumber!: number


  constructor(private tokenService : TokenStorageService, 
    private orderService: OrderService) { }

  ngOnInit(): void {

    this.user = this.tokenService.getUser()

    if(this.user.role == 'ROLE_RESELLER'){
      this.orderService.countResellersUnopenedOrders(this.user.reseller?.id!).subscribe(data => {
        this.notificationsNumber = data
      })
    }
    
  }


  logOut(){
    this.tokenService.signOut()
    window.location.reload()
  }
}
