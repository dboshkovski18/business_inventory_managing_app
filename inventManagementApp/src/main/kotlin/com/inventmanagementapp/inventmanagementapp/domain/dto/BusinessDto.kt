package com.inventmanagementapp.inventmanagementapp.domain.dto

data class BusinessDto(
    var business_id: Long?,
    val business_name: String?
)
