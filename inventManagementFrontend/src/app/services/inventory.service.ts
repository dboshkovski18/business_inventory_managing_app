import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Inventory } from '../interfaces/inventory';

@Injectable({
  providedIn: 'root'
})
export class InventoryService {
  
  path = '/api/inventories'


  constructor(private _http: HttpClient) { }

  getAllInventories() : Observable<Inventory[]> {
    return this._http.get<Inventory[]>(this.path.concat("/all"))    
  }


  getAllInventoriesByBusiness(id : number) : Observable<Inventory[]> {
    return this._http.get<Inventory[]>(this.path.concat(`/all-by-business/${id}`))    
  }

  createInventory(inventory: Inventory) : Observable<any> {
    return this._http.post<any>(this.path.concat('/create'), inventory)
  }

  deleteById(id : number) : Observable<any> {
    return this._http.delete<any>(this.path.concat(`/delete/${id}`))
  }

  editInventory(id : number, inventory: Inventory) : Observable<any> {
    return this._http.put<any>(this.path.concat(`/edit/${id}`), inventory)
  }

  findById(id: number) : Observable<Inventory> {
    return this._http.get<Inventory>(this.path.concat(`/${id}`))
  }

  checkIfBusinessHasProduct(business_id : number, product_id: number ) : boolean {

    var result : boolean = false
    

    this._http.get<boolean>(this.path.concat("/checkIfBusinessHasProduct"), { params : {business_id: business_id,product_id:product_id} }).subscribe(data => {
      result = data
    })

    return result

  }

  removeProductFromBusiness(business_id : number, product_id: number) : Observable<any> {
    return this._http.delete<any>(this.path.concat("/deleteProductFromBusiness"), { params : { business_id : business_id, product_id : product_id}})
  }

  updateQuantityOnStock(inventory_id: number, quantity: number): Observable<any>{
    return this._http.get<any>(this.path.concat("/updateQuantityOnStock"), {params : {inventory_id: inventory_id, quantity: quantity}})
  }
  
}
