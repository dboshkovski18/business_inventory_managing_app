import { Component, OnInit, ViewChild } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog'
import { CreateResellerComponent } from 'src/app/components/create-reseller/create-reseller.component';

@Component({
  selector: 'CreateProductDialog',
  templateUrl: './create-product.dialog.html',
  styleUrls: ['./create-product.dialog.css']
})
export class CreateProductDialog implements OnInit{

 

  constructor(public __dialogRef: MatDialogRef<CreateProductDialog>) { }

  ngOnInit(): void {

    
  }

  closeDialog() {
   this.__dialogRef.close()
  }

}
