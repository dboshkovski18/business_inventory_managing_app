package com.inventmanagementapp.inventmanagementapp.domain.dto

import javax.persistence.Entity

data class ProductResellerDto(
    val product_id: Long,
    val reseller_id: Long,
    val product_name: String,
    val reseller_name : String
)
