-- Insert data into the Business table
INSERT INTO businesses (business_name)
VALUES ('Business A'),
       ('Business B'),
       ('Business C');

-- Insert data into the Reseller table
INSERT INTO resellers (reseller_name, phone_number, brand_name)
VALUES ('Bunjakovec', '123-456-7890', 'Brand X'),
       ('Kvantaski', '987-654-3210', 'Brand Y');

-- Insert data into the Product table
INSERT INTO products (product_name, reseller_id)
VALUES ('Apple', 1),
       ('Orange', 2);

-- Insert data into the Inventory table
INSERT INTO inventories (business_id, product_id, quantity_limit)
VALUES (1, 1, 10),
       (1, 2, 5),
       (2, 1, 15),
       (3, 2, 8);

