CREATE TABLE orders (
                        id bigserial PRIMARY KEY,
                        product_id bigint,
                        quantity_ordered INT,
                        order_date DATE default now(),
                        urgency VARCHAR(20),
                        reseller_id bigint,
                        business_id bigint,
                        status VARCHAR(20) default 'NOT_OPENED',
                        FOREIGN KEY (product_id) REFERENCES products(id),
                        FOREIGN KEY (reseller_id) REFERENCES resellers(id),
                        FOREIGN KEY (business_id) REFERENCES businesses(id)
);
