import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Business } from '../interfaces/business';

@Injectable({
  providedIn: 'root'
})
export class BusinessService {

  path = '/api/businesses'

  constructor(private _http: HttpClient) { }

  getAllBusinesses() : Observable<Business[]> {
    return this._http.get<Business[]>(this.path.concat("/all"))    
  }

  findBusinessById(id: number) : Observable<Business>{
    return this._http.get<Business>(this.path.concat(`/${id}`))
  }
}
