package com.inventmanagementapp.inventmanagementapp.service

import com.inventmanagementapp.inventmanagementapp.domain.Business
import com.inventmanagementapp.inventmanagementapp.domain.Reseller
import com.inventmanagementapp.inventmanagementapp.domain.User
import com.inventmanagementapp.inventmanagementapp.domain.dto.UserDto
import com.inventmanagementapp.inventmanagementapp.repo.BusinessRepo
import com.inventmanagementapp.inventmanagementapp.repo.ResellerRepo
import com.inventmanagementapp.inventmanagementapp.repo.UserRepo
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service


@Service
class UserService(
    private val userRepo: UserRepo,
    private val businessRepo: BusinessRepo,
    private val passwordEncoder: PasswordEncoder,
    private val resellerRepo: ResellerRepo

) : UserDetailsService {


    fun findUserById(id: Long): User? {
        return this.userRepo.findById(id).get()
    }

    fun existsByUsername(username: String): Boolean {
        return this.userRepo.existsByUsername(username)
    }


    fun registerBusiness(
        userDto: UserDto?
    ): Any {

        userDto?.let {
            val business = this.businessRepo.save(
                Business(
                    business_name = userDto.business!!.business_name!!
                )
            )

            return this.userRepo.save(User(username = it.username!!, password = this.passwordEncoder.encode(it.password), role = it.role!!, business = business))
        }

        return "error"
    }

    fun registerReseller(
        userDto: UserDto?
    ): Any {

        userDto?.let {
            val reseller = this.resellerRepo.save(
                Reseller(
                    reseller_name = userDto.reseller!!.reseller_name!!,
                    phone_number = userDto.reseller!!.phone_number!!,
                    brand_name = userDto.reseller!!.brand_name!!
                )
            )

            return this.userRepo.save(User(username = it.username!!, password = this.passwordEncoder.encode(it.password), role = it.role!!, reseller = reseller))
        }

        return "error"
    }

    fun registerAdmin(
        userDto: UserDto?
    ): Any {
        return this.userRepo.save(User(username = userDto!!.username!!, password = this.passwordEncoder.encode(userDto!!.password), role = userDto!!.role!!))
    }

    override fun loadUserByUsername(username: String?): UserDetails {
        return this.userRepo.findUserByUsername(username!!)!!
    }
}