package com.inventmanagementapp.inventmanagementapp.api

import com.inventmanagementapp.inventmanagementapp.domain.Reseller
import com.inventmanagementapp.inventmanagementapp.domain.dto.ResellerDto
import com.inventmanagementapp.inventmanagementapp.service.ResellerService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/resellers")
class ResellerController(private val resellerService: ResellerService) {
    @GetMapping("/all")
    fun getAllResellers(): List<ResellerDto> {
        return resellerService.getAllResellers()
    }

    @GetMapping("/{resellerId}")
    fun getResellerById(@PathVariable resellerId: Long): ResellerDto? {
        return resellerService.getResellerById(resellerId)
    }

    @PostMapping("/create")
    fun createReseller(@RequestBody resellerDTO: ResellerDto): ResellerDto {
        return resellerService.createReseller(resellerDTO)
    }

    @PutMapping("/edit/{resellerId}")
    fun updateReseller(
        @PathVariable resellerId: Long,
        @RequestBody resellerDTO: ResellerDto
    ): ResellerDto {
        resellerDTO.reseller_id = resellerId
        return resellerService.updateReseller(resellerDTO)
    }

    @DeleteMapping("/delete/{resellerId}")
    fun deleteReseller(@PathVariable resellerId: Long) {
        resellerService.deleteReseller(resellerId)
    }

    @GetMapping("/search")
    fun searchResellersByNameOrBrand(@RequestParam term: String): List<ResellerDto?> =
        this.resellerService.searchResellersByNameOrBrand(term)
}