export interface Order {
        id? : number,  
        product : {
            id? : number,
           product_name: string,
            reseller: {
                id: number,
                reseller_name: string,
                phone_number: string,
                brand_name: string
            }
        },
        quantityOrdered : number,
        urgency: string,
        business :  {
            id: number,
            business_name: string
        },
        orderDate? : string,
        status? : string
        


}