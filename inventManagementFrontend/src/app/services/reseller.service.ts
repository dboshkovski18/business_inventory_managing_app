import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Reseller } from '../interfaces/reseller';

@Injectable({
  providedIn: 'root'
})
export class ResellerService {

  path = '/api/resellers'

  constructor(private _http: HttpClient) { }

  getAllResellers() : Observable<Reseller[]> {
    return this._http.get<Reseller[]>(this.path.concat("/all"))    
  }

  createReseller(reseller: Reseller) : Observable<any> {
    return this._http.post<any>(this.path.concat('/create'), reseller)
  }

  deleteById(id : number) : Observable<any> {
    return this._http.delete<any>(this.path.concat(`/delete/${id}`))
  }

  editReseller(id : number, reseller: Reseller) : Observable<any> {
    return this._http.put<any>(this.path.concat(`/edit/${id}`), reseller)
  }

  findById(id: number) : Observable<Reseller> {

    console.log(id)
    return this._http.get<Reseller>(this.path.concat(`/${id}`))
  }

  searchByTerm(term: string) : Observable<Reseller[]>{
    return this._http.get<Reseller[]>(`${this.path}/search?term=${term}`)
  }
}
