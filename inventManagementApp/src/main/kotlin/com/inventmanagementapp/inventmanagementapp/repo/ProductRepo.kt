package com.inventmanagementapp.inventmanagementapp.repo

import com.inventmanagementapp.inventmanagementapp.domain.Product
import com.inventmanagementapp.inventmanagementapp.domain.dto.ProductResponse
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.persistence.ColumnResult
import javax.persistence.ConstructorResult
import javax.persistence.SqlResultSetMapping

@Repository
interface ProductRepo : JpaRepository<Product, Long> {


    @Query(
        value = "SELECT p.id as productId ,p.product_name as productName,  p.reseller_id as resellerId, (i.quantity_on_stock IS NOT NULL) as inStock \n" +
                "FROM\n" +
                "    products p\n" +
                "        LEFT JOIN inventories i ON p.id = i.product_id AND i.business_id = :businessId ;", nativeQuery = true
    )
    fun findAllProductsByBusiness(businessId: Long):List<ProductResponse>

    @Query("SELECT p FROM Product p WHERE LOWER(p.product_name) LIKE %:searchValue%")
    fun searchProductsByName(searchValue: String) : List<Product?>

}