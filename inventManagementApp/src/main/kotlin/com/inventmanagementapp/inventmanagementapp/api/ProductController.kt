package com.inventmanagementapp.inventmanagementapp.api

import com.inventmanagementapp.inventmanagementapp.domain.Product
import com.inventmanagementapp.inventmanagementapp.domain.dto.ProductDto
import com.inventmanagementapp.inventmanagementapp.domain.dto.ProductResponse
import com.inventmanagementapp.inventmanagementapp.service.ProductService
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/products")
class ProductController(private val productService: ProductService) {

    @GetMapping("/all")
    fun getAllProducts(): List<ProductDto> {
        return productService.getAllProducts()
    }

    @GetMapping("/{productId}")
    fun getProductById(@PathVariable productId: Long): ProductDto? {
        return productService.getProductById(productId)
    }

    @PostMapping("/create")
    fun createProduct(@RequestBody productDTO: ProductDto): ProductDto {
        return productService.createProduct(productDTO)
    }

    @PutMapping("/edit/{productId}")
    fun updateProduct(
        @PathVariable productId: Long,
        @RequestBody productDTO: ProductDto
    ): ProductDto {
        productDTO.id = productId
        return productService.updateProduct(productDTO)
    }

    @DeleteMapping("/delete/{productId}")
    fun deleteProduct(@PathVariable productId: Long) {
        productService.deleteProduct(productId)
    }

    @GetMapping("/get-by-business/{id}")
    fun getAllProductsForBusiness(@PathVariable id: Long): List<ProductResponse> =
        this.productService.findAllProductsByBusiness(id)

    @GetMapping("/search")
    fun searchProductsByName(@RequestParam term: String, @RequestParam business_id :Long): List<ProductResponse?> =
        this.productService.searchProductsByName(term, business_id )
}

