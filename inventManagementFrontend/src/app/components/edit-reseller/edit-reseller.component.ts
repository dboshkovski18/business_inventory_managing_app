import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ResellerService } from 'src/app/services/reseller.service';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog'
import { EditResellerDialog } from 'src/app/dialogs/edit-reseller.dialog/edit-reseller.dialog';
import { Reseller } from 'src/app/interfaces/reseller';

@Component({
  selector: 'app-edit-reseller',
  templateUrl: './edit-reseller.component.html',
  styleUrls: ['./edit-reseller.component.css']
})
export class EditResellerComponent implements OnInit {

  editResellerForm! : FormGroup



  loader: boolean = false
  errorMessage: boolean = false
  succesMessage: boolean = false
  
  resellerId! : number

  @Input() reseller! : Reseller

  @Input() __dialogRef! : MatDialogRef<EditResellerDialog>



  constructor(private resellerService: ResellerService ){ }

  ngOnInit(): void {
  

    this.editResellerForm = new FormGroup({
      reseller_name : new FormControl(this.reseller.reseller_name),
      phone_number: new FormControl(this.reseller.phone_number),
      brand_name : new FormControl(this.reseller.brand_name)
    })

    this.resellerId = this.reseller.reseller_id
  }

  editReseller() {
    console.log(this.editResellerForm.value)

    this.resellerService.editReseller(this.resellerId, this.editResellerForm.value).subscribe(()=> {},() => {
      this.errorMessage = true
    },() => {

      this.loader = false
      this.succesMessage = true
    
      
    })

    this.__dialogRef.close()
  }

  
}
