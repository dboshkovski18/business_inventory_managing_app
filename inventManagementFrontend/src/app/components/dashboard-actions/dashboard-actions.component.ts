import { Component, OnInit } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { CreateProductDialog } from 'src/app/dialogs/create-product.dialog /create-product.dialog';
import { CreateResellerDialog } from 'src/app/dialogs/create-reseller.dialog/create-reseller.dialog';
import { User } from 'src/app/interfaces/user';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-dashboard-actions',
  templateUrl: './dashboard-actions.component.html',
  styleUrls: ['./dashboard-actions.component.css']
})
export class DashboardActionsComponent implements OnInit {

  user! : User

  constructor(private dialog : MatDialog,
    private tokenService: TokenStorageService) { }

  ngOnInit(): void {
    this.user = this.tokenService.getUser()
  }

  quickAddNewReseller() {
    this.dialog.open(CreateResellerDialog)
  }

  quickAddNewProduct() {
    this.dialog.open(CreateProductDialog)
  }
}
