import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../interfaces/product';
import { ProductResponse } from '../interfaces/product-response';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  
  path = '/api/products'

  constructor(private _http: HttpClient) { }

  getAllProducts() : Observable<Product[]> {
    return this._http.get<Product[]>(this.path.concat("/all"))    
  }

  createProduct(product: Product) : Observable<any> {
    return this._http.post<any>(this.path.concat('/create'), product)
  }

  deleteById(id : number) : Observable<any> {
    return this._http.delete<any>(this.path.concat(`/delete/${id}`))
  }

  editProduct(id : number, product: Product) : Observable<any> {
    return this._http.put<any>(this.path.concat(`/edit/${id}`), product)
  }

  findById(id: number) : Observable<Product> {
    return this._http.get<Product>(this.path.concat(`/${id}`))
  }

  findAllByBusiness(business_id: number) : Observable<ProductResponse[]>{
    return this._http.get<ProductResponse[]>(this.path.concat(`/get-by-business/${business_id}`))
  }

  searchProductsByName(term : string, business_id : number) : Observable<ProductResponse[]> {
    return this._http.get<ProductResponse[]>(`${this.path}/search?term=${term}&business_id=${business_id}`)
  }
}
