package com.inventmanagementapp.inventmanagementapp.service

import com.inventmanagementapp.inventmanagementapp.domain.Product
import com.inventmanagementapp.inventmanagementapp.domain.dto.ProductDto
import com.inventmanagementapp.inventmanagementapp.domain.dto.ProductResponse
import com.inventmanagementapp.inventmanagementapp.domain.dto.ResellerDto
import com.inventmanagementapp.inventmanagementapp.repo.ProductRepo
import com.inventmanagementapp.inventmanagementapp.repo.ResellerRepo
import javax.persistence.EntityManager
import org.springframework.stereotype.Service
import javax.persistence.Query

@Service
class ProductService(
    private val productRepository: ProductRepo,
    private val resellerRepo: ResellerRepo,
    private val entityManager: EntityManager
) {

    fun createProduct(productDTO: ProductDto): ProductDto {
        val product = Product(
            product_name = productDTO.product_name!!,
            reseller = this.resellerRepo.findById(productDTO.reseller!!.reseller_id!!).get()
        )
        val savedProduct = productRepository.save(product)
        return ProductDto(
            id = savedProduct.id,
            product_name = savedProduct.product_name,
            reseller =  ResellerDto(savedProduct.reseller.id,savedProduct.reseller.reseller_name,savedProduct.reseller.phone_number,savedProduct.reseller.brand_name )
        )
    }

    fun getProductById(productId: Long): ProductDto? {
        val product = productRepository.findById(productId).orElse(null)
        return product?.let {
            ProductDto(
                id = it.id,
                product_name = it.product_name,
                reseller = ResellerDto(it.reseller.id,it.reseller.reseller_name,it.reseller.phone_number,it.reseller.brand_name )
            )
        }
    }

    fun getAllProducts(): List<ProductDto> {
        val products = productRepository.findAll()
        return products.map {
            ProductDto(
                id = it.id,
                product_name = it.product_name,
                reseller  = ResellerDto(it.reseller.id,it.reseller.reseller_name,it.reseller.phone_number,it.reseller.brand_name )

            )
        }
    }

    fun updateProduct(productDTO: ProductDto): ProductDto {
        val product = Product(
            id = productDTO.id!!,
            product_name = productDTO.product_name!!,
            reseller =  this.resellerRepo.findById(productDTO.reseller!!.reseller_id!!).get()
        )
        val updatedProduct = productRepository.save(product)
        return ProductDto(
            id = updatedProduct.id,
            product_name = updatedProduct.product_name,
            reseller =  ResellerDto(updatedProduct.reseller.id,updatedProduct.reseller.reseller_name,updatedProduct.reseller.phone_number,updatedProduct.reseller.brand_name )
        )
    }

    fun deleteProduct(productId: Long) {
        productRepository.deleteById(productId)
    }

    fun getProductsByIds(productIDs: List<Long>): List<Product> {
        return productRepository.findAllById(productIDs)
    }

    fun findAllProductsByBusiness(business_id: Long): List<ProductResponse>{
//        return this.productRepository.findAllProductsByBusiness(business_id)

        val nativeQuery: Query = entityManager.createNativeQuery(
            "SELECT p.id , p.reseller_id, " +
                    "CASE WHEN i.quantity_on_stock IS NOT NULL THEN true ELSE false END AS inStock " +
                    "FROM products p " +
                    "LEFT JOIN inventories i ON p.id = i.product_id AND i.business_id = :businessId"
        )
        nativeQuery.setParameter("businessId", business_id)

        val result = nativeQuery.resultList
        val productResponses = result.map { row ->
            val productId = (row as Array<Any>)[0].toString().toLong()
            val resellerId = (row as Array<Any>)[1].toString().toLong()

            val inStock = row[2] as Boolean
            ProductResponse(this.productRepository.findById(productId).get(), this.resellerRepo.findById(resellerId).get(), inStock)
        }

        return productResponses
    }

    fun searchProductsByName(term : String, business_id: Long) : List<ProductResponse?> {

        if(term != ""){

            val query : Query = entityManager.createQuery("SELECT p.id, p.reseller.id," +
                    "CASE WHEN i.quantity_on_stock IS NOT NULL THEN true ELSE false END AS inStock  " +
                    "FROM Product p " +
                    "LEFT JOIN Inventory i"+
                    " ON p.id = i.product.id AND i.business.id = :businessId" +
                    " WHERE LOWER(p.product_name) LIKE concat('%' , :searchValue, '%')")



            query.setParameter("businessId", business_id)
            query.setParameter("searchValue", term.toLowerCase())

            val result = query.resultList
            val productResponses = result.map { row ->
                val productId = (row as Array<Any>)[0].toString().toLong()
                val resellerId = (row as Array<Any>)[1].toString().toLong()

                val inStock = row[2] as Boolean
                ProductResponse(this.productRepository.findById(productId).get(), this.resellerRepo.findById(resellerId).get(), inStock)
            }

            return productResponses
        }else{
            return this.findAllProductsByBusiness(business_id)
        }

    }


}
