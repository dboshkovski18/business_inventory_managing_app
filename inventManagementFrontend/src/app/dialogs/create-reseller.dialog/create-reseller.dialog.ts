import { Component, OnInit, ViewChild } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog'
import { CreateResellerComponent } from 'src/app/components/create-reseller/create-reseller.component';

@Component({
  selector: 'CreateResellerDialog',
  templateUrl: './create-reseller.dialog.html',
  styleUrls: ['./create-reseller.dialog.css']
})
export class CreateResellerDialog implements OnInit{

 

  constructor(public __dialogRef: MatDialogRef<CreateResellerDialog>) { }

  ngOnInit(): void {

    
  }

  closeDialog() {
   this.__dialogRef.close()
  }

}
