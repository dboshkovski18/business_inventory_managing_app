import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { Inventory } from 'src/app/interfaces/inventory';
import { Order } from 'src/app/interfaces/order';
import { OrderService } from 'src/app/services/order.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-order.dialog',
  templateUrl: './order.dialog.html',
  styleUrls: ['./order.dialog.css']
})
export class OrderDialog implements OnInit {

  inventory! : Inventory

  urgencyForm! : FormGroup

  constructor(private _dialogRef : MatDialogRef<OrderDialog>,
    @Inject(MAT_DIALOG_DATA) data : any,
    private tokenService: TokenStorageService,
        private orderService: OrderService) { 

      this.inventory = data.inventory

    }

  ngOnInit(): void {

    this.urgencyForm = new FormGroup({
      urgency : new FormControl('STANDARD')
    })
  }


  automaticOrder(){

    console.log(this.inventory)
    var quantiyToOrder : number = this.inventory.quantity_limit - this.inventory.quantity_on_stock

    var product  ={
      id: this.inventory.product.id,
      product_name: this.inventory.product.product_name,
      reseller: {
        id: this.inventory.product.reseller.reseller_id,
        reseller_name: this.inventory.product.reseller.reseller_name,
        phone_number: this.inventory.product.reseller.phone_number,
        brand_name: this.inventory.product.reseller.brand_name
      }
    }

    var order : Order = {
      quantityOrdered: quantiyToOrder,
      urgency: this.urgencyForm.value.urgency,
      business: {
        id: this.tokenService.getUser().business?.id!,
        business_name: this.tokenService.getUser().business?.business_name!
      },
      product: product
    }

    this.orderService.processOrder(order).subscribe(()=> {}, () => {}, () => {
      this._dialogRef.close()
    })
    
  }
}
