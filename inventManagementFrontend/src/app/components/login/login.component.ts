import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthorizationService } from 'src/app/services/authorization.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  error : boolean = false;

  loginForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  })

  constructor(private authService: AuthorizationService, private tokenStorage: TokenStorageService
    , private router: Router) {
  }

  ngOnInit(): void {

  }

  login() {
    this.authService.logIn(this.loginForm.value.username!, this.loginForm.value.password!).subscribe(
      res => {
        this.tokenStorage.saveToken(res.access_token);
        this.tokenStorage.saveUser(res.user);
      
      },
      err => this.error = true,
      () => 
      {


        this.router.navigate(['/dashboard'])

      }
    )
  }

}
