package com.inventmanagementapp.inventmanagementapp.domain.dto

import com.inventmanagementapp.inventmanagementapp.domain.Business
import com.inventmanagementapp.inventmanagementapp.domain.enum.Role

data class UserDto(
    val username: String? = null,

    val password: String? = null,

    val role: Role? = null,

    val business : BusinessDto?,

    val reseller : ResellerDto?
)
