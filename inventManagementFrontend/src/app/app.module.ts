import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { BusinessComponent } from './components/business/business.component';
import { DashboardPage } from './pages/dashboard/dashboard.page';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { DashboardActionsComponent } from './components/dashboard-actions/dashboard-actions.component';
import { ResellersComponent } from './components/resellers/resellers.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CreateResellerDialog } from './dialogs/create-reseller.dialog/create-reseller.dialog';
import { MatLegacyDialogModule as MatDialogModule } from '@angular/material/legacy-dialog';
import { CreateResellerComponent } from './components/create-reseller/create-reseller.component';
import { DeleteResellerDialog } from './dialogs/delete-reseller.dialog/delete-reseller.dialog';
import { ConfirmDialog } from './dialogs/confirm-dialog/confirm.dialog'
import { EditResellerDialog } from './dialogs/edit-reseller.dialog/edit-reseller.dialog';
import { EditResellerComponent } from './components/edit-reseller/edit-reseller.component';
import { ProductsComponent } from './components/products/products/products.component';
import { CreateProductDialog } from './dialogs/create-product.dialog /create-product.dialog';
import { CreateProductComponent } from './components/create-product/create-product.component';
import { EditProductComponent } from './components/edit-product/edit-product.component';
import { EditProductDialog } from './dialogs/edit-product.dialog/edit-product.dialog';
import { CreateInventoryDialog } from './dialogs/create-inventory.dialog /create-inventory.dialog';
import { CreateInventoryComponent } from './components/create-inventory/create-inventory.component';
import { UpdateStockInventoryDialog } from './dialogs/edit-quantity.dialog /edit-quantity.dialog';
import { LoginComponent } from './components/login/login.component';
import { AuthInterceptor, authInterceptorProviders } from './interceptors/authInterceptor';
import { ResellerProfileDialog } from './components/reseller-profile/reseller-profile.dialog';
import { RegisterComponent } from './components/register/register.component';
import { ResellerProductsComponent } from './components/reseller-products/reseller-products.component';
import { OrderDialog } from './dialogs/order.dialog/order.dialog';
import { InventoriesComponent } from './components/inventories/inventories.component';
import { OrdersComponent } from './components/orders/orders.component';

const modules = [
  BrowserModule,
  AppRoutingModule,
  HttpClientModule,
  ReactiveFormsModule,
  BrowserAnimationsModule,
  MatDialogModule,
  FormsModule
]

const components = [
    AppComponent,
    BusinessComponent, 
    NavBarComponent, 
    SidebarComponent, 
    DashboardActionsComponent,
    ResellersComponent,
    CreateResellerComponent,
    EditResellerComponent,
    ProductsComponent,
    CreateProductComponent,
    EditProductComponent,
    InventoriesComponent,
    CreateInventoryComponent,
    LoginComponent,
    RegisterComponent, 
    ResellerProductsComponent
]

const dialogs = [
  CreateResellerDialog,
  DeleteResellerDialog,
  ConfirmDialog,
  EditResellerDialog,
  CreateProductDialog,
  EditProductDialog,
  CreateInventoryDialog,
  UpdateStockInventoryDialog,
  ResellerProfileDialog,
  OrderDialog
]

const interceptors = [
  authInterceptorProviders
]
const pages = [ 
  DashboardPage
]



@NgModule({
  declarations: [
    ...components, ...pages, ...dialogs, OrdersComponent, 
  ],
  imports: [
   ...modules
  ],
  providers: [
   ...interceptors,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
