package com.inventmanagementapp.inventmanagementapp.domain

import com.inventmanagementapp.inventmanagementapp.domain.composite_key.ProductResellerID
import javax.persistence.*

@Entity
@Table(name = "product_reseller_view")
class ProductReseller(
    @EmbeddedId
    val id: ProductResellerID? = null,

    val product_name: String,

    val reseller_name: String
)