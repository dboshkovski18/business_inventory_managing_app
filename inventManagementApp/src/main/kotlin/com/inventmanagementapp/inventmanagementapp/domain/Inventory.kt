package com.inventmanagementapp.inventmanagementapp.domain


import com.inventmanagementapp.inventmanagementapp.domain.enum.InventoryStatus
import javax.persistence.*

@Entity
@Table(name = "inventories")
data class Inventory(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id : Long = 0,

    @ManyToOne
    @JoinColumn(name = "business_id")
    val business: Business,

    @ManyToOne
    @JoinColumn(name = "product_id")
    val product: Product,

    @Enumerated(value = EnumType.STRING)
    var status : InventoryStatus = InventoryStatus.NOT_ORDERED,

    var quantity_limit : Int,

    var quantity_on_stock: Int?

){

}
