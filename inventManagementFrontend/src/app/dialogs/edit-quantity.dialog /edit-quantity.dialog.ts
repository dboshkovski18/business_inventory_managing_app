import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog'
import { CreateResellerComponent } from 'src/app/components/create-reseller/create-reseller.component';
import { Inventory } from 'src/app/interfaces/inventory';
import { Product } from 'src/app/interfaces/product';
import { Reseller } from 'src/app/interfaces/reseller';
import { InventoryService } from 'src/app/services/inventory.service';

@Component({
  selector: 'UpdateStockInventoryDialog',
  templateUrl: './edit-quantity.dialog.html',
  styleUrls: ['./edit-quantity.dialog.css']
})
export class UpdateStockInventoryDialog implements OnInit{

  inventory! : Inventory

  editQuantityForm! : FormGroup

  constructor(public __dialogRef: MatDialogRef<UpdateStockInventoryDialog>,
              @Inject(MAT_DIALOG_DATA) data : any,
              private inventoryService : InventoryService) { 

                this.inventory = data.inventory
              }

  ngOnInit(): void {

    this.editQuantityForm = new FormGroup({
      quantity: new FormControl(this.inventory.quantity_on_stock,[
        Validators.required,
        Validators.min(0)
      ])
    })
  }

  editInventory() {
    this.inventoryService.updateQuantityOnStock(this.inventory.inventory_id,this.editQuantityForm.value.quantity).subscribe(()=> {},
    ()=>{console.log("Unsuccesfull update!")},
    ()=> {
      this.closeDialog()
      console.log("Succesfully updated!")})
  }

  closeDialog() {
   this.__dialogRef.close()
  }

}
