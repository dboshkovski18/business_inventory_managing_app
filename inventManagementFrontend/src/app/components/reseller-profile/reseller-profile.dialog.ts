import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatLegacyDialog as MatDialog, MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ConfirmDialog } from 'src/app/dialogs/confirm-dialog/confirm.dialog';
import { CreateInventoryDialog } from 'src/app/dialogs/create-inventory.dialog /create-inventory.dialog';
import { Product } from 'src/app/interfaces/product';
import { ProductResponse } from 'src/app/interfaces/product-response';
import { Reseller } from 'src/app/interfaces/reseller';
import { User } from 'src/app/interfaces/user';
import { BusinessService } from 'src/app/services/business.service';
import { InventoryService } from 'src/app/services/inventory.service';
import { ProductResellerService } from 'src/app/services/product-reseller.service';
import { ResellerService } from 'src/app/services/reseller.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-reseller-profile',
  templateUrl: './reseller-profile.dialog.html',
  styleUrls: ['./reseller-profile.dialog.css']
})
export class ResellerProfileDialog implements OnInit {

  reseller!: Reseller
  productsByReseller! : Observable<Product[]>

  user! :User

  loader! : boolean

  constructor(private route: ActivatedRoute,
    private resellerService : ResellerService,
    private productResellerService: ProductResellerService,
    private dialog : MatDialog,
    private inventoryService: InventoryService,
    private businessService: BusinessService,
    private tokenService: TokenStorageService,
    private _dialogRef : MatDialogRef<ResellerProfileDialog>,
    @Inject(MAT_DIALOG_DATA) data : any ) {
     
    this.reseller = data.reseller

  }
  

  ngOnInit(): void {

    this.route.paramMap.subscribe(params => {
      // Get the 'id' parameter from the URL
      this.resellerService.findById(this.reseller.reseller_id).subscribe(data=> 
        {
          this.findAllProductsByResellerName(data.reseller_name)
        })
    });

  }
  closeDialog(){
    this._dialogRef.close()
  }

  findAllProductsByResellerName(name : string) {
    this.productsByReseller = this.productResellerService.findAllProductsByResellerName(name)
  }


  addToBusiness(product : Product){
    
    this.dialog.open(CreateInventoryDialog, { data : { product : product }})
    .afterClosed()
    .subscribe(complete => {
      this.loader = true
      this.findAllProductsByResellerName(this.reseller.reseller_name)
    })
  }

  removeFromBusiness(product : Product) {


    console.log(this.inventoryService.checkIfBusinessHasProduct(this.user.business!!.id!!,product.id))



    this.dialog.open(ConfirmDialog,{
      data : { 
        message: "Are you sure you want to delete this reseller?",
        confirm_message: "Yes",
        decline_message: "No"
      }
      })
    .afterClosed()
    .subscribe( data => {
      if(data){
        this.inventoryService.removeProductFromBusiness(this.user.business!!.id!!,product.id).subscribe(complete => {
          this.loader = true
          this.findAllProductsByResellerName(this.reseller.reseller_name)
        })
      }
      
        })
  }
}
