package com.inventmanagementapp.inventmanagementapp.domain.config

import com.inventmanagementapp.inventmanagementapp.domain.config.filters.JWTAuthenticationFilter
import com.inventmanagementapp.inventmanagementapp.domain.config.filters.JwtAuthorizationFilter
import com.inventmanagementapp.inventmanagementapp.service.UserService
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
class JWTWebSecurityConfig(
    val passwordEncoder: PasswordEncoder,
    val userService: UserService
) : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity?) {
        http!!.cors().and().csrf().disable()
            .authorizeRequests()
            .antMatchers("/api/register", "/login").permitAll()
            .anyRequest()
            .authenticated()
            .and()
            .addFilter(JWTAuthenticationFilter(authenticationManager(),this.userService,this.passwordEncoder))
            .addFilter(JwtAuthorizationFilter(authenticationManager(),userService))
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }
}