import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Order } from 'src/app/interfaces/order';
import { OrderService } from 'src/app/services/order.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {


  ordersForReseller! : Order[]

  constructor(private orderService: OrderService,
    private tokenService: TokenStorageService) { }

  ngOnInit(): void {

    this.loadAllOrders()
  }

  pageSize = 5; // Number of items per page
  currentPage = 1;

  get paginatedItems() {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    return this.ordersForReseller.slice(startIndex, endIndex);
  }

  changePage(pageNumber: number) {
    this.currentPage = pageNumber;
  }

  getPageNumbers() {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(this.ordersForReseller.length / this.pageSize); i++) {
      pageNumbers.push(i);
    }
    return pageNumbers;
  }


  loadAllOrders(){
     this.orderService.findAllOrdersByReseller(this.tokenService.getUser().reseller?.id!).subscribe(data => {
      this.ordersForReseller = data
     })
  }

  markAsOpen(order_id : number){
    this.orderService.markOrderAsOpened(order_id).subscribe(complete => {
      window.location.reload()
    })
  }

}
