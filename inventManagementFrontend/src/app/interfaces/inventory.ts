import { Business } from "./business";
import { Product } from "./product";

export interface Inventory {
    inventory_id: number,
    quantity_limit: number,
    business: Business,
    product: Product,
    quantity_on_stock : number,
    status : string
}
