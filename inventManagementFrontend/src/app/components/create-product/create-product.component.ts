import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CreateResellerDialog } from 'src/app/dialogs/create-reseller.dialog/create-reseller.dialog';
import { ResellerService } from 'src/app/services/reseller.service';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog'
import { ProductService } from 'src/app/services/product.service';
import { Reseller } from 'src/app/interfaces/reseller';
import { Product } from 'src/app/interfaces/product';
import { delay, pipe } from 'rxjs';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  createProductForm! : FormGroup

  resellers! : Reseller[]

  @Input() __dialogRef! : MatDialogRef<CreateResellerDialog>

  loader: boolean = false
  errorMessage: boolean = false
  succesMessage: boolean = false



  constructor(private resellerService: ResellerService,
    private productService: ProductService,
    private tokenService: TokenStorageService ){ }

  ngOnInit(): void {
    this.resellerService.getAllResellers().subscribe((data)=> {
      this.resellers = data
    })
    this.createProductForm = new FormGroup({
      product_name : new FormControl('')
    })
  }

  createProduct() {
    console.log(this.createProductForm.value  )

    this.loader = true

    this.resellerService.findById(this.tokenService.getUser().reseller?.id!).subscribe(data => {
      console.log(data)
      var product : Product = {
        product_name: this.createProductForm.value.product_name,
        reseller: data,
        id: 0
      }

      this.productService.createProduct(product).subscribe(()=>{},() => {
        this.errorMessage = true
      },() => {

        this.loader = false
        this.succesMessage = true
        this.__dialogRef.close()
      })
    })
   

    
  }

  
}
