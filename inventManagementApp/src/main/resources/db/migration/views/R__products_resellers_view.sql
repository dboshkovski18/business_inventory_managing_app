-- Create the Product_Reseller junction table for Many-to-Many relationship
drop view if exists product_reseller_view;

CREATE VIEW product_reseller_view AS
SELECT
    products.id as product_id,
    products.product_name,
    resellers.id as reseller_id,
    resellers.reseller_name
FROM
    products
        JOIN
    resellers ON products.reseller_id = resellers.id;

