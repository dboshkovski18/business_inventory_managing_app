import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog'
import { CreateResellerComponent } from 'src/app/components/create-reseller/create-reseller.component';
import { Product } from 'src/app/interfaces/product';
import { Reseller } from 'src/app/interfaces/reseller';

@Component({
  selector: 'EditProductDialog',
  templateUrl: './edit-product.dialog.html',
  styleUrls: ['./edit-product.dialog.css']
})
export class EditProductDialog implements OnInit{

  product! : Product

  constructor(public __dialogRef: MatDialogRef<EditProductDialog>,
              @Inject(MAT_DIALOG_DATA) data : any) { 

                this.product = data.product
              }

  ngOnInit(): void {

    
  }

  closeDialog() {
   this.__dialogRef.close()
  }

}
