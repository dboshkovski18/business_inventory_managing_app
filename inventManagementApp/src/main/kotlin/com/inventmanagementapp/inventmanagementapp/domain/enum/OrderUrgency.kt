package com.inventmanagementapp.inventmanagementapp.domain.enum

enum class OrderUrgency {

    STANDARD,
    URGENT

}