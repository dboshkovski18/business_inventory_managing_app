import { Component, OnInit } from '@angular/core';
import { Form, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthorizationService } from 'src/app/services/authorization.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private authService: AuthorizationService,
    private router  : Router) { }

  registerBusinessForm! : FormGroup

  registerResellerForm! : FormGroup

  ngOnInit(): void {

    this.registerBusinessForm = new FormGroup({
      username : new FormControl(),
      password: new FormControl(),
      business_name: new FormControl()
    })

    this.registerResellerForm = new FormGroup({
      username : new FormControl(),
      password: new FormControl(),
      reseller_name: new FormControl(),
      brand_name: new FormControl(),
      phone_number: new FormControl()
    })
  }


  registerBusiness(){
    this.authService.registerBusiness(this.registerBusinessForm.value).subscribe(()=>{},() => {},() => {
      this.router.navigate(['/dashboard'])   
     })
  }

  registerReseller(){
    this.authService.registerReseller(this.registerResellerForm.value).subscribe(()=>{},()=>{},()=>{
      this.router.navigate(['/dashboard'])
    })
  }
}
