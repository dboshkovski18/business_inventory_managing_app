import { Component, OnInit } from '@angular/core';
import { Business } from 'src/app/interfaces/business';
import { BusinessService } from 'src/app/services/business.service';

@Component({
  selector: 'app-business',
  templateUrl: './business.component.html',
  styleUrls: ['./business.component.css']
})
export class BusinessComponent implements OnInit {

  constructor(private businessService: BusinessService) { }

  businesses! : Business[]

  ngOnInit(): void {
    this.businessService.getAllBusinesses().subscribe(data => {
      this.businesses = data
    })
  }



}
