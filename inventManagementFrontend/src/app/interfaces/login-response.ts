import {User} from "./user";

export interface LogInResponse{
  access_token: string,
  user: User
}
