package com.inventmanagementapp.inventmanagementapp.repo

import com.inventmanagementapp.inventmanagementapp.domain.Business
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface BusinessRepo : JpaRepository<Business, Long>{
}