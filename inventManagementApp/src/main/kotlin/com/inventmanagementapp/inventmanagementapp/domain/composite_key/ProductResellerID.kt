package com.inventmanagementapp.inventmanagementapp.domain.composite_key

import javax.persistence.Embeddable
import java.io.Serializable

@Embeddable
data class ProductResellerID(
    val product_id: Long,
    val reseller_id: Long,
) : Serializable
