package com.inventmanagementapp.inventmanagementapp.api

import com.inventmanagementapp.inventmanagementapp.domain.Order
import com.inventmanagementapp.inventmanagementapp.domain.dto.OrderDto
import com.inventmanagementapp.inventmanagementapp.service.OrderService
import org.apache.tomcat.util.json.JSONParser
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate
import java.time.format.DateTimeFormatter


@RestController
@RequestMapping("/api/orders")
class OrderContoller(private val orderService: OrderService) {

    @PostMapping
    fun processOrder(@RequestBody order: OrderDto): OrderDto {
        return this.orderService.processOrder(order)
    }

    @GetMapping("/by-reseller/{id}")
    fun findAllOrdersByReseller(@PathVariable id: Long): List<Order> = this.orderService.findAllOrdersByReseller(id)

    @GetMapping("/by-business/{id}")
    fun findAllOrdersByBusiness(@PathVariable id: Long): List<Order> = this.orderService.findAllByBusiness(id)


    @GetMapping("/open/{order_id}")
    fun markOrderAsOpened(@PathVariable order_id: Long) = this.orderService.markOrderAsOpened(order_id)

    @GetMapping("/not-opened/{resellerId}")
    fun countNotOpenedByReseller(@PathVariable resellerId: Long) =
        this.orderService.countNotOpenedByReseller(resellerId)

    @GetMapping("/filter-by-date")
    fun findAllByBusinessIdAndOrderDate(@RequestParam business_id: Long, @RequestParam date: String): List<Order> {

        var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

        return this.orderService.findAllByBusinessIdAndOrderDate(business_id, LocalDate.parse(date, formatter))
    }
}