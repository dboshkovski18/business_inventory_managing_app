import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog'
import { CreateResellerComponent } from 'src/app/components/create-reseller/create-reseller.component';
import { Reseller } from 'src/app/interfaces/reseller';

@Component({
  selector: 'EditResellerDialog',
  templateUrl: './edit-reseller.dialog.html',
  styleUrls: ['./edit-reseller.dialog.css']
})
export class EditResellerDialog implements OnInit{

  reseller! : Reseller

  constructor(public __dialogRef: MatDialogRef<EditResellerDialog>,
              @Inject(MAT_DIALOG_DATA) data : any) { 

                this.reseller = data.reseller
              }

  ngOnInit(): void {

    
  }

  closeDialog() {
   this.__dialogRef.close()
  }

}
