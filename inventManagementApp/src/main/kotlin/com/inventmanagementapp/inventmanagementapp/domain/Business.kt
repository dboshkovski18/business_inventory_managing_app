package com.inventmanagementapp.inventmanagementapp.domain

import javax.persistence.*


@Entity
@Table(name = "businesses")
data class Business(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id : Long = 0,

    var business_name : String,
)
