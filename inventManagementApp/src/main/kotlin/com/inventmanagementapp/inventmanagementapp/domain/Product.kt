package com.inventmanagementapp.inventmanagementapp.domain

import javax.persistence.*

@Entity
@Table(name="products")
data class Product(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id : Long = 0,


    val product_name: String,

    @ManyToOne
    @JoinColumn(name = "reseller_id")
    val reseller: Reseller
)
