import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardActionsComponent } from './components/dashboard-actions/dashboard-actions.component';
import { InventoriesComponent } from './components/inventories/inventories.component';
import { LoginComponent } from './components/login/login.component';
import { OrdersComponent } from './components/orders/orders.component';
import { ProductsComponent } from './components/products/products/products.component';
import { RegisterComponent } from './components/register/register.component';
import { ResellerProductsComponent } from './components/reseller-products/reseller-products.component';
import { ResellersComponent } from './components/resellers/resellers.component';
import { AuthGuard } from './guards/auth.guard';
import { BusinessGuard } from './guards/business.guard';
import { ResellerGuard } from './guards/reseller.guard';
import { DashboardPage } from './pages/dashboard/dashboard.page';

const routes: Routes = [
  {
    path : 'welcome',
    component: LoginComponent
  },
  {
    path : 'register',
    component : RegisterComponent
  },
  {
    
    path: 'dashboard',
    component : DashboardPage,
    canActivate: [AuthGuard],
    children : [
      {
        path : 'reseller-orders',
        component  : OrdersComponent,
        canActivate: [ResellerGuard]
      },
      {
        path : 'reseller-products',
        component  : ResellerProductsComponent,
        canActivate: [ResellerGuard]
      },
      {
        path : 'inventory',
        component  : InventoriesComponent,
        canActivate: [BusinessGuard]
      },
      {
        path : 'products',
        component  : ProductsComponent,
        canActivate: [BusinessGuard]
      },

      {
        path : 'resellers',
        component  : ResellersComponent,
      },
      {
        path : '',
        component : DashboardActionsComponent
      }
    ]
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
