import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CreateResellerDialog } from 'src/app/dialogs/create-reseller.dialog/create-reseller.dialog';
import { ResellerService } from 'src/app/services/reseller.service';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog'

@Component({
  selector: 'app-create-reseller',
  templateUrl: './create-reseller.component.html',
  styleUrls: ['./create-reseller.component.css']
})
export class CreateResellerComponent implements OnInit {

  createResellerForm! : FormGroup


  loader: boolean = false
  errorMessage: boolean = false
  succesMessage: boolean = false

  @Input() __dialogRef! : MatDialogRef<CreateResellerDialog>



  constructor(private resellerService: ResellerService ){ }

  ngOnInit(): void {

    this.createResellerForm = new FormGroup({
      reseller_name : new FormControl(''),
      phone_number: new FormControl(''),
      brand_name : new FormControl('')
    })
  }

  createReseller() {
    console.log(this.createResellerForm.value)

    this.resellerService.createReseller(this.createResellerForm.value).subscribe(()=> {},() => {
      this.errorMessage = true
    },() => {

      this.loader = false
      this.succesMessage = true
   
    })

    this.__dialogRef.close()
  }

  
}
