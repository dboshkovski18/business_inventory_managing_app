import com.inventmanagementapp.inventmanagementapp.domain.Product
import com.inventmanagementapp.inventmanagementapp.domain.Reseller
import javax.persistence.*

@Entity
@Table(name = "reseller_products")
class ResellerProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private val id: Long? = null

    @ManyToOne
    @JoinColumn(name = "reseller_id")
    private val reseller: Reseller? = null

    @ManyToOne
    @JoinColumn(name = "product_id")
    private val product: Product? = null // Constructors, getters, and setters
}
