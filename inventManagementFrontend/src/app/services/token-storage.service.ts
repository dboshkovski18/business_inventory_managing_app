import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';


const TOKEN_KEY = 'auth-token'
const USER_KEY = 'auth-user'

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }

  signOut(){
    window.sessionStorage.clear()
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY)
    window.sessionStorage.setItem(TOKEN_KEY,token)
  }

  public getToken(): string {
    return <string>sessionStorage.getItem(TOKEN_KEY)
  }

  public saveUser(user: User) {
    window.sessionStorage.removeItem(USER_KEY)
    window.sessionStorage.setItem(USER_KEY,JSON.stringify(user))
  }

  public getUser(): User{
    return JSON.parse(<string>sessionStorage.getItem(USER_KEY))
  }

  public isLoggedIn() {

    const token = this.getToken()

    return token

  }

  public isReseller(){
    
    if(this.getUser().role == 'ROLE_RESELLER' || this.getUser().role == 'ROLE_ADMIN'){
      return true
    }

    return false


  }


  public isBusiness(){
    
    if(this.getUser().role == 'ROLE_BUSINESS' || this.getUser().role == 'ROLE_ADMIN'){
      return true
    }

    return false


  }


}