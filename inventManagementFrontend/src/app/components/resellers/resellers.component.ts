import { Component, OnChanges, OnInit } from '@angular/core';
import { debounceTime, distinctUntilChanged, Observable, Subject, switchMap, tap } from 'rxjs';
import { Reseller } from 'src/app/interfaces/reseller';
import { ResellerService } from 'src/app/services/reseller.service';
import { MatLegacyDialogRef as MatDialogRef, MatLegacyDialog as MatDialog} from '@angular/material/legacy-dialog'
import { CreateResellerDialog } from 'src/app/dialogs/create-reseller.dialog/create-reseller.dialog';
import { DeleteResellerDialog } from 'src/app/dialogs/delete-reseller.dialog/delete-reseller.dialog';
import { ConfirmDialog } from 'src/app/dialogs/confirm-dialog/confirm.dialog';
import { EditResellerDialog } from 'src/app/dialogs/edit-reseller.dialog/edit-reseller.dialog';
import { ResellerProfileDialog } from '../reseller-profile/reseller-profile.dialog';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-resellers',
  templateUrl: './resellers.component.html',
  styleUrls: ['./resellers.component.css']
})
export class ResellersComponent implements OnInit {

  resellers! : Reseller[]

  pageSize = 3; // Number of items per page
  currentPage = 1;

  private searchTerms = new Subject<string>()
  searchForm! : FormGroup

  loader! : boolean


  errorMessage : boolean = false

  constructor(private resellerService : ResellerService
    ,private dialog : MatDialog) {

      this.searchForm = new FormGroup({
        searchTerm : new FormControl()

      })

      this.searchForm.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(term => {
          this.loader = true
          return this.resellerService.searchByTerm(this.searchForm.value.searchTerm)

        })
      )
      .subscribe(data => {
        console.log(data)
        this.resellers = data
        if(data.length == 0){
          this.errorMessage = true
        }else{
          this.errorMessage = false
        }

        this.loader = false
      },()=>{})
    }

  ngOnInit(): void {
    this.loader = true
    this.loadAllResellers()
  }




  get paginatedItems() {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    return this.resellers.slice(startIndex, endIndex);
  }

  changePage(pageNumber: number) {
    this.currentPage = pageNumber;
  }

  getPageNumbers() {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(this.resellers.length / this.pageSize); i++) {
      pageNumbers.push(i);
    }
    return pageNumbers;
  }




  loadAllResellers() {
    this.resellerService.getAllResellers().subscribe(data => {
      this.resellers = data
    }, ()=>{} ,() => {
      this.loader = false
    })
  }

  addResellerDialog() {
    this.dialog
    .open(CreateResellerDialog)
    .afterClosed().subscribe(() => this.loadAllResellers())
  }

  askToDelete(id : number) {
    this.dialog
    .open(ConfirmDialog, {
      data : {
        message: "Are you sure you want to delete this reseller?",
        confirm_message: "Yes",
        decline_message: "No"
      }
      })
    .afterClosed()
    .subscribe( data => {
      if(data){
        this.resellerService.deleteById(id).subscribe(() => {
          this.loader = true
          this.loadAllResellers()
        })
      }
    })
  }


  editReseller(reseller : Reseller) {
    this.dialog.open(EditResellerDialog, {
      data : {
        reseller: reseller
      }
    })
    .afterClosed()
    .subscribe(() => {
      this.loadAllResellers()
    })
  }


  openViewResellerProfileDialog(reseller : Reseller){


    this.dialog.open(ResellerProfileDialog, {
      data : { reseller : reseller}})
  }



  searchByTerm(term: string) {
    this.loader = true
    this.resellerService.searchByTerm(term).subscribe(data => {
      this.resellers = data
      if(data.length == 0){
        this.errorMessage = true
      }else{
        this.errorMessage = false
      }
    }, () => {
    },() => {
      this.loader = false
    })
  }
}
