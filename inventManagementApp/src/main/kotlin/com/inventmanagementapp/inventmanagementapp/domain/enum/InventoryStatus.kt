package com.inventmanagementapp.inventmanagementapp.domain.enum

enum class InventoryStatus {

    ORDERED,
    NOT_ORDERED

}