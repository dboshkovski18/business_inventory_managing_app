package com.inventmanagementapp.inventmanagementapp.domain.dto

import com.inventmanagementapp.inventmanagementapp.domain.Business
import com.inventmanagementapp.inventmanagementapp.domain.Product

data class InventoryDto(var inventory_id: Long?,
                        val quantity_limit: Int?,
                        val quantity_on_stock: Int?,
                        val business: BusinessDto?,
                        val product: ProductDto?,
                        val status : String)
