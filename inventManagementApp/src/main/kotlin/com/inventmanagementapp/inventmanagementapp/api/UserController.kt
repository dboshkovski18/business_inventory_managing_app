package com.inventmanagementapp.inventmanagementapp.api

import com.inventmanagementapp.inventmanagementapp.domain.dto.LoginRequest
import com.inventmanagementapp.inventmanagementapp.domain.dto.UserDto
import com.inventmanagementapp.inventmanagementapp.service.UserService
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/api")
class UserController (private val userService: UserService) {

    @PostMapping("/register")
    fun registerUser(@RequestBody user: UserDto):Any{
        when(user.role!!.name) {
            "ROLE_BUSINESS" -> return this.userService.registerBusiness(user)
            "ROLE_RESELLER" -> return this.userService.registerReseller(user)
            "ROLE_ADMIN" -> return this.userService.registerAdmin(user)
            else -> {
                return "Unsuccessful register"
            }
        }
    }

}