package com.inventmanagementapp.inventmanagementapp.service

import com.inventmanagementapp.inventmanagementapp.domain.Business
import com.inventmanagementapp.inventmanagementapp.domain.dto.BusinessDto
import com.inventmanagementapp.inventmanagementapp.repo.BusinessRepo
import org.springframework.stereotype.Service

@Service
class BusinessService (private val businessRepository: BusinessRepo) {

    fun createBusiness(businessDTO: BusinessDto): BusinessDto {
        val business = businessDTO.business_name?.let {
            Business(
                business_name = it
            )
        }
        val savedBusiness = businessRepository.save(business!!)
        return BusinessDto(
            business_id = savedBusiness.id,
            business_name = savedBusiness.business_name
        )
    }

    fun getBusinessById(businessId: Long): BusinessDto? {
        val business = businessRepository.findById(businessId).orElse(null)
        return business?.let {
            BusinessDto(
                business_id = it.id,
                business_name = it.business_name
            )
        }
    }

    fun getAllBusinesses(): List<BusinessDto> {
        val businesses = businessRepository.findAll()
        return businesses.map {
            BusinessDto(
                business_id = it.id,
                business_name = it.business_name
            )
        }
    }

    fun updateBusiness(businessDTO: BusinessDto): BusinessDto {
        val business = Business(
            id = businessDTO.business_id!!,
            business_name = businessDTO.business_name!!
        )
        val updatedBusiness = businessRepository.save(business)
        return BusinessDto(
            business_id = updatedBusiness.id,
            business_name = updatedBusiness.business_name
        )
    }

    fun deleteBusiness(businessId: Long) {
        businessRepository.deleteById(businessId)
    }
}
