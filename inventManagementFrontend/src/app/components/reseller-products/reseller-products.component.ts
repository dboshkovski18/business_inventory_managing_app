import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { debounceTime, distinctUntilChanged, Observable, Subject, switchMap } from 'rxjs';
import { ConfirmDialog } from 'src/app/dialogs/confirm-dialog/confirm.dialog';
import { CreateProductDialog } from 'src/app/dialogs/create-product.dialog /create-product.dialog';
import { EditProductDialog } from 'src/app/dialogs/edit-product.dialog/edit-product.dialog';
import { Product } from 'src/app/interfaces/product';
import { User } from 'src/app/interfaces/user';
import { ProductResellerService } from 'src/app/services/product-reseller.service';
import { ProductService } from 'src/app/services/product.service';
import { ResellerService } from 'src/app/services/reseller.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-reseller-products',
  templateUrl: './reseller-products.component.html',
  styleUrls: ['./reseller-products.component.css']
})
export class ResellerProductsComponent implements OnInit {

  products! : Product[]
  user! : User

  loader! : boolean

  pageSize = 5; // Number of items per page
  currentPage = 1;

  private searchTerms = new Subject<string>()
  searchForm! : FormGroup


  errorMessage : boolean = false


  constructor(private tokenService: TokenStorageService, 
    private productResellerService: ProductResellerService,
    private resellerService: ResellerService,
    private productService: ProductService,
    private dialog: MatDialog) { 
      this.searchForm = new FormGroup({
        searchTerm : new FormControl()

      })

      this.searchForm.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        switchMap(term => {
          this.loader = true 
          return this.productResellerService.searchProductsByReseller(this.searchForm.value.searchTerm, this.user.reseller?.id!!)
          
        })
      )
      .subscribe(data => {
        console.log(data)
        this.products = data
        if(data.length == 0){
          this.errorMessage = true
        }else{
          this.errorMessage = false
        }

        this.loader = false
      },()=>{})
    }

  
  


    

  ngOnInit(): void {

    this.user = this.tokenService.getUser()

    this.findAllProductByReseller()

  }

  searchByTerm(term : string){
    this.productResellerService.searchProductsByReseller(term, this.user.reseller?.id!!).subscribe(data => {
      this.products = data
    })
  }
 
  get paginatedItems() {
    const startIndex = (this.currentPage - 1) * this.pageSize;
    const endIndex = startIndex + this.pageSize;
    return this.products.slice(startIndex, endIndex);
  }

  changePage(pageNumber: number) {
    this.currentPage = pageNumber;
  }

  getPageNumbers() {
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(this.products.length / this.pageSize); i++) {
      pageNumbers.push(i);
    }
    return pageNumbers;
  }


  findAllProductByReseller() {
    this.productResellerService.findAllProductsByResellerName(this.user.reseller?.reseller_name!).subscribe(data => {
      this.products =data
    })
    this.loader = false
  }

  openAddProductDialog() {
    this.dialog
    .open(CreateProductDialog)
    .afterClosed().subscribe(() => {
      this.loader = true
      this.findAllProductByReseller()
    })
  } 



  askToDelete(id : number) {
    this.dialog
    .open(ConfirmDialog, {
      data : { 
        message: "Are you sure you want to delete this reseller?",
        confirm_message: "Yes",
        decline_message: "No"
      }
      })
    .afterClosed()
    .subscribe( data => {
      if(data){
        this.productService.deleteById(id).subscribe(() => {
          this.loader = true
          this.findAllProductByReseller()
        })
      }
    })
  }


  editProduct(product : Product) {
    this.dialog.open(EditProductDialog, {
      data : {
        product: product
      }
    })
    .afterClosed()
    .subscribe(() => {
      this.loader = true
      this.findAllProductByReseller()
    })
  } 

}
