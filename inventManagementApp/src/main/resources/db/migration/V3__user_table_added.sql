CREATE TABLE IF NOT EXISTS users
(
    id       bigserial primary key,
    username text unique,
    password text,
    role     text,
    is_account_not_expired bool default true,
    is_account_non_locked bool default  true,
    is_credentials_not_expired bool default true,
    is_enabled bool default true,
    business_id bigint,
    reseller_id bigint,
    constraint pk_business
        foreign key (business_id)
            references businesses (id) ON DELETE CASCADE ON UPDATE CASCADE,
    constraint pk_reseller
        foreign key (reseller_id)
            references resellers (id) ON DELETE CASCADE ON UPDATE CASCADE
);