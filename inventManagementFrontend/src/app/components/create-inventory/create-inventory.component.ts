import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CreateResellerDialog } from 'src/app/dialogs/create-reseller.dialog/create-reseller.dialog';
import { ResellerService } from 'src/app/services/reseller.service';
import { MatLegacyDialogRef as MatDialogRef } from '@angular/material/legacy-dialog'
import { ProductService } from 'src/app/services/product.service';
import { Reseller } from 'src/app/interfaces/reseller';
import { Product } from 'src/app/interfaces/product';
import { delay, pipe } from 'rxjs';
import { InventoryService } from 'src/app/services/inventory.service';
import { BusinessService } from 'src/app/services/business.service';
import { Inventory } from 'src/app/interfaces/inventory';
import { TokenStorageService } from 'src/app/services/token-storage.service';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-create-inventory',
  templateUrl: './create-inventory.component.html',
  styleUrls: ['./create-inventory.component.css']
})
export class CreateInventoryComponent implements OnInit {

  createInventoryForm! : FormGroup


  @Input() __dialogRef! : MatDialogRef<CreateResellerDialog>

  @Input () product!: Product


  constructor(private resellerService: ResellerService,
    private productService: ProductService,
    private inventoryService: InventoryService,
    private businessService : BusinessService,
    private tokenService: TokenStorageService ){   

    }

  ngOnInit(): void {

 


    this.createInventoryForm = new FormGroup({
      quantity_limit : new FormControl('')
    })
  }

  createInventory() {
    console.log(this.createInventoryForm.value  )

    console.log(this.tokenService.getUser())

    var business_id : number = this.tokenService.getUser().business!!.id!!

    this.businessService.findBusinessById(business_id).subscribe(data => {
      var inventory : Inventory = {
        inventory_id: 0,
        quantity_on_stock: 0,
        quantity_limit: this.createInventoryForm.value.quantity_limit,
        business: data,
        product: this.product,
        status: ''
      }

      this.inventoryService.createInventory(inventory).subscribe(complete => this.__dialogRef.close())

    
  } )

  
  }

}
